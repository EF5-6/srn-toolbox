#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Implements an Estimation of Mixture of vMF Distributions Algorithm."""

import logging
import random

import numpy as np

from itertools import count

# HACK: Disable annoying output from importing geomstats.
logger = logging.getLogger()
logger.disabled = True

from geomstats.geometry.hypersphere import Hypersphere  # noqa
from geomstats.learning.kmeans import RiemannianKMeans  # noqa

logger.disabled = False


class EMMFA:
    """Estimation of Mixture of von Mises-Fisher Distributions Algorithm.

    An Estimation of Distribution Algorithm (EDA) on hyperspherical support
    that is designed to optimize highly multimodal functions.

    The distribution being estimated is an integral-weight mixture of von
    Mises-Fisher distributions, where the mixture is based on an external
    hyperspherical k-means clustering of the best individuals of a generation.
    The number and location of clusters is adapted as the algorithm fails to
    make progress. More precisely, whenever the mean solution value has not
    improved with respect to the last generation, we attempt to split the
    cluster with the lowest concentration by sampling two initial k-means
    centroids from its distribution (while renewing all other clusters by using
    their distribution's mean direction as an initial centroid).
    """

    class Convergence(RuntimeError):
        """An exception raised by :meth:`estimate` upon convergence."""

        pass

    class Cluster:
        """Represents a cluster/distribution."""

        def __init__(self, mu, kappa, weight):  # noqa
            self.mu = mu
            self.kappa = kappa
            self.weight = weight
            self.num_survivors = 0

    class GenerationStats:
        """Contains statistics for one generation."""

        def __init__(
            self, sorted_population, sorted_fitness, used_clusters
        ):  # noqa
            self.population = sorted_population
            self.fitness = sorted_fitness
            self.used_clusters = used_clusters
            self.next_clusters = None

    def __init__(
        self,
        sphere_dim,
        function,
        *,
        maximize=False,
        population_factor=50,
        survival_factor=0.5,
    ):
        """Initialize the algorithm.

        :param int sphere_dim:
            Dimension d of the hyperspherical search space. This is one less
            than the dimension of the real space that contains the sphere.

        :param function:
            A function that receives as input a NumPy array of shape (n, d + 1)
            where n is the population size and d the dimension of the search
            space (coordinates are (d + 1)-dimensional) and that returns a
            vector of fitness values corresponding to each row of the array.

        :param maximize:
            Whether to maximize the function instead of minimizing it.

        :param int population_factor:
            The population size is set to this factor times the search space
            dimension.

        :param float survival_factor:
            Fraction of the population to re-estimate the model with.
        """
        self._S = Hypersphere(sphere_dim)
        self._d = sphere_dim
        self._f = function
        self._maximize = maximize
        self._n = round(sphere_dim * population_factor)
        self._s = round(self._n * survival_factor)

        bad = -np.inf if maximize else np.inf

        self._clusters = [self.Cluster(*self._estimate(), weight=1)]
        self._survivors = None
        self._made_progress = None
        self._best_individual = None
        self._best_value = bad
        self._last_mean_value = bad
        self._history = []

        # NOTE: Rejection sampling is used starting with the 3-sphere, and this
        #       can cause numeric issues around kappa = 1e8.
        self._max_kappa = np.inf if self._d <= 2 else 9e7

    def _estimate(self, samples=None):
        # Return a uniform distribution if no samples are provided.
        if samples is None or samples.shape[0] == 0:
            mu = np.zeros(self._d + 1, dtype=float)
            mu[0] = 1.0

            return mu, 0.0

        # Normalize samples.
        samples = samples / np.linalg.norm(samples, axis=1)[:, np.newaxis]

        # Handle the case of a single sample.
        if samples.shape[0] == 1:
            return samples.flatten(), np.inf

        # Compute the mean sample and its norm.
        mean = np.mean(samples, axis=0)
        norm = np.linalg.norm(mean)
        sqnorm = norm**2

        # Make an (approximate) maximum likelihood estimate of the parameters.
        # High-dimensional estimate for kappa first described in "Clustering on
        # the Unit Hypersphere using von Mises-Fisher Distributions." (Banerjee
        # et al. 2005; J. Mach. Learn. Res. 6).
        mu = mean / norm
        kappa = (
            np.inf if sqnorm >= 1 else norm * (self._d - sqnorm) / (1 - sqnorm)
        )

        return mu, kappa

    def _sample(self, mu, kappa, k):
        if not kappa:
            return self._S.random_uniform(n_samples=k)

        # NOTE: For trivial/test functions and in higher dimensions, this is
        #       the performance bottleneck (> 90% of time spent).
        return self._S.random_von_mises_fisher(mu=mu, kappa=kappa, n_samples=k)

    def evaluate(self):
        """Sample and evaluate a generation.

        1. Sample a generation of individuals from the current mixture model.
        2. Evaluate the individuals.
        3. Select the fittest individuals as survivors.
        4. Store with the mixture model the number of survivors each
           distribution produced (for use in the mixture estimation routine).
        5. Update the best individual seen and store generation statistics.
        """
        clusters = self._clusters

        # Assign a fraction of the offspring population to each cluster
        # according to its weight.
        num_children = np.zeros(len(clusters), dtype=int)
        parent_cluster = np.zeros(self._n, dtype=int)
        for position, cluster in enumerate(
            random.choices(
                population=range(len(clusters)),
                weights=[cluster.weight for cluster in clusters],
                k=self._n,
            )
        ):
            num_children[cluster] += 1
            parent_cluster[position] = cluster

        # Sample offspring from all clusters.
        offspring = np.vstack(
            [
                self._sample(mu=cluster.mu, kappa=cluster.kappa, k=count)
                for cluster, count in zip(clusters, num_children)
                if count
            ]
        )

        assert len(offspring) == self._n

        # Compute fitness for the offspring.
        fitness = self._f(offspring)

        # Order offspring and its metadata with decreasing quality.
        order = fitness.argsort()
        order = order[::-1] if self._maximize else order
        offspring = offspring[order]
        fitness = fitness[order]
        parent_cluster = parent_cluster[order]

        # Select survivors.
        self._survivors = offspring[: self._s]

        # Annotate clusters for the estimation step.
        for cluster_num in parent_cluster[: self._s]:
            clusters[cluster_num].num_survivors += 1

        new_best_value = fitness[0]
        new_mean_value = np.mean(fitness)

        # Update best individual seen.
        if (self._maximize and new_best_value > self._best_value) or (
            not self._maximize and new_best_value < self._best_value
        ):
            self._best_individual = offspring[0]
            self._best_value = new_best_value

        # Determine if progress was made.
        self._made_progress = (
            self._maximize and new_mean_value > self._last_mean_value
        ) or (not self._maximize and new_mean_value < self._last_mean_value)

        # Update evolution history.
        self._last_mean_value = new_mean_value
        self._history.append(
            self.GenerationStats(
                sorted_population=offspring,
                sorted_fitness=fitness,
                used_clusters=clusters,
            )
        )

    def estimate(self):
        """Estimate a new mixture model from old model and survivors.

        1. Remove from the last mixture model all distributions that produced
           no survivors.
        2. Obtain from the remaining distributions a set of initial centroids:

           - Normally, use each distribution's mean direction as one centroid.
           - If the algorithm made no progress between the last two rounds of
             evaluation, select the lowest concentration distribution and,
             instead of using its mean direction, sample two centroids from it.

        3. Cluster the survivors using k-means with initial centroids given.
        4. Estimate a distribution for each resulting cluster of survivors.
        5. Remove distributions whose parameters exceed a convergence treshold.
        6. If no distribution remains, terminate the algorithm.
        7. Mix the remaining distributions according to the number of survivors
           in each cluster.
        """
        # Acquire this Generation's survivors.
        survivors = self._survivors

        if survivors is None:
            raise RuntimeError("Need to evaluate the initial estimate first.")

        assert len(survivors) == self._s

        # Filter clusters that produced no survivors.
        old_clusters = [c for c in self._clusters if c.num_survivors]

        assert old_clusters, "No cluster has produced a survivor."

        # Obtain initial centroids for new clusters.
        if self._made_progress:
            # Maintain number of (non-useless) clusters and use last cluster
            # centers as initial centroids for the next clustering.
            centroids = np.vstack([c.mu for c in old_clusters])
        else:
            # Did not make progress: Split the lowest concentration cluster by
            # sampling two centroids from its distribution.
            widest_cluster = min(old_clusters, key=lambda x: x.kappa)
            centroids = np.vstack(
                [
                    self._sample(cluster.mu, cluster.kappa, 2)
                    if cluster is widest_cluster
                    else cluster.mu
                    for cluster in old_clusters
                ]
            )

        # Partition survivors via k-means using initial centroids.
        num_clusters = centroids.shape[0]
        if num_clusters > 1:
            rkm = RiemannianKMeans(
                n_clusters=num_clusters, space=self._S, init=centroids
            )
            rkm.fit(survivors)
            labels = rkm.predict(survivors)
        else:
            labels = np.zeros(self._s, dtype=int)

        # Estimate a distribution for each new cluster. Remove clusters whose
        # distributional parameters signify convergence.
        clusters = []
        for label in set(labels):
            group = survivors[labels == label]
            size = group.shape[0]

            mu, kappa = self._estimate(group)

            if kappa >= self._max_kappa:
                continue

            # Use cluster training sample size as preliminary weight.
            clusters.append(self.Cluster(mu=mu, kappa=kappa, weight=size))

        # Store clusters.
        self._clusters = clusters
        self._history[-1].next_clusters = clusters

        # Enforce termination upon convergence.
        if not clusters:
            raise self.Convergence()

    def step(self):
        """Evaluate a generation and update the mixture estimate."""
        self.evaluate()
        self.estimate()

    # TODO: Add a time budget.
    def run(self, *, max_generations=None, verbose=True):
        """Execute the algorithm.

        :param int or None max_generations:
            Maximum number of generations to evaluate.

        :param bool verbose:
            Whether to report progress to the console.
        """
        if verbose:
            print(
                f"Running {self.__class__.__name__} "
                f"on a {self._d}-sphere with "
                f"population={self._n}, "
                f"survivors={self._s}, "
                f"max_generations={max_generations}."
            )

        for generation in count(1):
            try:
                self.step()
            except self.Convergence:
                converged = True
            else:
                converged = False

            kappas = [c.kappa for c in self._clusters]
            if not kappas:
                kappas = [0]

            if verbose:
                print(
                    f"# {generation:03d} "
                    f"{'≤' if self._maximize else '≥'} {self._best_value:.3e} "
                    f"Ø {self._last_mean_value:.3e} "
                    f"C {len(self._clusters):03d} "
                    f"κ {min(kappas):.1e} – {max(kappas):.1e}"
                )

            if converged:
                if verbose:
                    print("Stopping as all clusters have converged.")
                break

            if max_generations and generation >= max_generations:
                if verbose:
                    print("Stopping after maximum number of generations.")
                break

        return self._best_value, self._best_individual


if __name__ == "__main__":
    # Run the algorithm on a toy problem and visualize the process (for d=2).

    def cost_sum(samples):
        """Compute a convex function."""
        minimum = -samples.shape[1] / np.sqrt(samples.shape[1])
        return -np.sum(samples, axis=1) - minimum

    def cost_onenorm(samples):
        """Compute a function with 2*(d+1) global minima."""
        return np.linalg.norm(samples, ord=1, axis=1) - 1

    def cost_infnorm(samples):
        """Compute a function with 2^(d+1) global minima."""
        minimum = 1 / np.sqrt(samples.shape[1])
        return np.linalg.norm(samples, ord=np.inf, axis=1) - minimum

    COST = lambda x: (cost_infnorm(x) + cost_onenorm(x))  # noqa
    DIM = 2
    GEN = 30

    eda = EMMFA(DIM, COST)
    value, solution = eda.run(max_generations=GEN)

    print(f"Solution: {solution}")

    if DIM == 2:
        import plotly.graph_objects as go

        def sphere_trace(func=None, *, resolution=100, colorscale="viridis"):
            """Draw a function evaluated over a sphere."""
            theta = np.linspace(0, 2 * np.pi, resolution)
            phi = np.linspace(0, np.pi, resolution)

            x = np.outer(np.cos(theta), np.sin(phi))
            y = np.outer(np.sin(theta), np.sin(phi))
            z = np.outer(np.ones(resolution), np.cos(phi))

            d = np.stack([x, y, z], axis=-1)
            d = d.reshape(-1, d.shape[-1])
            c = func(d).reshape(z.shape)

            trace = go.Surface(
                x=x,
                y=y,
                z=z,
                surfacecolor=c,
                colorscale=colorscale,
                lighting=dict(diffuse=1, specular=0.1),
            )

            return trace

        buttons = [
            dict(
                label="Play",
                method="animate",
                args=[None, dict(fromcurrent=True)],
            ),
            dict(
                label="Pause",
                method="animate",
                args=[[None], dict(mode="immediate")],
            ),
            dict(
                label="Reset",
                method="animate",
                args=[["1"], dict(mode="immediate")],
            ),
        ]

        steps = [
            dict(
                label=i + 1,
                method="animate",
                args=[
                    [i + 1],
                    dict(frame=dict(redraw=True), mode="immediate"),
                ],
            )
            for i in range(len(eda._history))
        ]

        frames = [
            go.Frame(
                name=f"{i + 1}",
                data=[
                    go.Scatter3d(
                        mode="markers",
                        x=x,
                        y=y,
                        z=z,
                        marker=dict(
                            size=10,
                            symbol="cross",
                            color=["blue"] * eda._s
                            + ["yellow"] * (eda._n - eda._s),
                        ),
                        showlegend=False,
                    ),
                    go.Scatter3d(
                        mode="markers",
                        x=[cluster.mu[0] for cluster in used_clusters],
                        y=[cluster.mu[1] for cluster in used_clusters],
                        z=[cluster.mu[2] for cluster in used_clusters],
                        marker=dict(size=10, color="lightgrey"),
                        showlegend=False,
                    ),
                    go.Scatter3d(
                        mode="markers",
                        x=[cluster.mu[0] for cluster in next_clusters],
                        y=[cluster.mu[1] for cluster in next_clusters],
                        z=[cluster.mu[2] for cluster in next_clusters],
                        marker=dict(size=10, color="orange"),
                        showlegend=False,
                    ),
                ],
            )
            for i, ((x, y, z), used_clusters, next_clusters) in enumerate(
                (g.population.T, g.used_clusters, g.next_clusters)
                for g in eda._history
            )
        ]

        fig = go.Figure(
            data=list(frames[0]["data"]) + [sphere_trace(COST)],
            layout=go.Layout(
                title=(
                    "Estimation of Adaptive Mixture of von "
                    "Mises-Fisher Distributions Algorithm (EMMFA)"
                ),
                scene=dict(
                    xaxis=dict(range=[-1, 1]),
                    yaxis=dict(range=[-1, 1]),
                    zaxis=dict(range=[-1, 1]),
                ),
                updatemenus=[
                    dict(type="buttons", buttons=buttons),
                ],
                sliders=[
                    dict(
                        active=0,
                        currentvalue=dict(prefix="Generation "),
                        steps=steps,
                    )
                ],
            ),
            frames=frames,
        )

        fig.show()
