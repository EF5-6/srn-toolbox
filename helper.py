#!/usr/bin/env python3
# pylama:ignore=D203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Collection of helper functions."""

import pickle
import random
from contextlib import contextmanager
from time import perf_counter

import numpy as np
import pandas as pd


class Timer:
    """Time the context."""

    def __init__(self):  # noqa
        self.reset()

    def reset(self):  # noqa
        self.elapsed = 0.0
        self.things_done = 0
        self.batches_done = 0

    @contextmanager
    def measure(self, number_things=0, count_batch=True):  # noqa
        start = perf_counter()
        yield
        self.elapsed += perf_counter() - start
        if number_things:
            self.things_done += number_things
        if count_batch:
            self.batches_done += 1

    @property
    def time_per_thing(self):  # noqa
        return self.elapsed / self.things_done

    @property
    def time_per_batch(self):  # noqa
        return self.elapsed / self.batches_done


def seed_prngs(seed):
    """Seed all relevant PRNGs."""
    random.seed(seed)
    np.random.seed(random.randrange(2**32))


def load_network(file):
    """Load a networkx network from a pickled file."""
    with open(file, "rb") as f:
        network = pickle.load(f)

    network.graph["file"] = str(file)

    return network


def load_points(file):
    """Load a pandas point dataframe from a JSON file."""
    points = pd.read_json(file)

    points.attrs["file"] = str(file)

    return points


def principal_minor(A, i):
    """Return a principal minor of a numpy matrix."""
    m, n = A.shape

    keep_rows = np.array(list(j for j in range(m) if i != j), dtype="int")

    if m == n:
        keep_cols = keep_rows
    else:
        keep_cols = np.array(list(j for j in range(n) if i != j), dtype="int")

    return A[keep_rows[:, np.newaxis], keep_cols]
