#!/usr/bin/env python3
# pylama:ignore=D213

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Helper functions to load a GRASS GIS vector map as a pandas dataframe."""

from ast import literal_eval

import pandas as pd
from grass.pygrass.vector import VectorTopo
from grass.pygrass.vector.table import DBlinks

_ATTR_NUM_ATTRS = "num_attrs"  # Number of values for each feature attribute.
_ATTR_POINT_COORDS = "coords"  # Geographic coordinates of a point.
_ATTR_POINT_COORDS_X = "x"  # First coordinate of a point.
_ATTR_POINT_COORDS_Y = "y"  # Second coordinate of a point.
_ATTR_POINT_COORDS_Z = "z"  # Potential third coordinate of a point.


def gg2pd_points(
    map_name,
    mapset=None,
    layer=None,
    *,
    strict_attributes=False,
    eval_attributes=None,
    allow_empty=False,
    extra_attribute_prefix="_",
):
    """Load a GRASS GIS pointvector map with pandas.

    :param str map_name:
        Name of the GRASS vector map containing the points.

    :param str mapset:
        Mapset that contains the vector map, or :obj:`None` for current mapset.

    :param int layer:
        Number of the layer containing point attributes, or ``0``/:obj:`None`.
        If a point has multiple (no) categories, then the associated attributes
        are stored as a :obj:`tuple` (as :obj:`None`), unless this is forbidden
        via ``strict_attributes``.

    :param bool allow_empty:
        Whether a vector map without points will be loaded as an empty
        dataframe. Otherwise, a :exc:`ValueError` is raised in this case.

    :param bool strict_attributes:
        Whether to ensure that every point has exactly one category value. If
        this is enabled and any point feature has zero or multiple categories,
        then a :exc:`ValueError` is raised.

    :param list(str) eval_attributes:
        A sequence of column names of the point attribute table whose values
        are strings and should be evaluated using :func:`ast.literal_eval`.

    :param str extra_attribute_prefix:
        In addition to those obtained using the ``layer`` argument, this
        function adds further columns to the returned dataframe. To avoid name
        clashes, this string is used as a prefix for the names of these
        additional attributes.
    """
    if not eval_attributes:
        eval_attributes = ()

    with VectorTopo(map_name, mapset, mode="r") as features:
        num_points = features.number_of("points")

        if not allow_empty and not num_points:
            raise ValueError("No points in <%s>." % map_name)

        if layer:
            db_links = DBlinks(features.c_mapinfo)
            db_link = db_links.by_layer(layer)

            if not db_link:
                raise LookupError(
                    "There is no database associated with layer %d. Set "
                    "layer=None to load the points without attriutes." % layer
                )

            db_table = db_link.table()
            db_header = db_table.columns.names()

            if "cat" not in db_header:
                raise KeyError(
                    "The attribute table does not have a 'cat' column."
                )

            if any(name not in db_header for name in eval_attributes):
                raise KeyError("Not all attributes to evaluate found.")

            db_table.filters.where("cat IS NOT null")
            db_data = dict(
                (
                    row[db_header.index("cat")],
                    tuple(
                        literal_eval(attr)
                        if db_header[col] in eval_attributes
                        else attr
                        for col, attr in enumerate(row)
                    ),
                )
                for row in db_table
            )

        table = []
        for point in features.viter("points"):
            if layer:
                cats = sorted(
                    point.c_cats.contents.cat[i]
                    for i in range(point.c_cats.contents.n_cats)
                )
                n = len(cats)

                if strict_attributes and n != 1:
                    raise ValueError(
                        "The point with ID %d has %d categories "
                        "but strict_attributes=True." % (point.id, len(cats))
                    )

                try:
                    if not cats:
                        row = (None,) * len(db_header)
                    elif n == 1:
                        row = db_data[cats[0]]
                    else:
                        row = zip(db_data[cat] for cat in cats)
                except KeyError as error:
                    raise ValueError(
                        "Could not retrieve attributes for all vertices. "
                        "Does the database table associated with the given "
                        "layer have a row for each point category?"
                    ) from error

                row = dict(zip(*(db_header, row)))
                row[extra_attribute_prefix + _ATTR_NUM_ATTRS] = n
            else:
                row = {}

            # NOTE: These are the only line specific to point features.
            # TODO: Consider extending this function for other feature types.
            coords = point.coords()

            if len(coords) == 2:
                (x, y), z = coords, None
            elif len(coords) == 3:
                x, y, z = coords
            else:
                x, z, z = (None,) * 3

            row[extra_attribute_prefix + _ATTR_POINT_COORDS] = coords
            row[extra_attribute_prefix + _ATTR_POINT_COORDS_X] = x
            row[extra_attribute_prefix + _ATTR_POINT_COORDS_Y] = y
            row[extra_attribute_prefix + _ATTR_POINT_COORDS_Z] = z

            table.append(row)

    return pd.DataFrame(table)


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(
        description="Load a GRASS GIS point map in Python and display it."
    )
    parser.add_argument("map", metavar="MAP", help="vector map to load")
    parser.add_argument(
        "-m",
        dest="mapset",
        metavar="MAPSET",
        help="mapset to use",
        default=None,
    )
    parser.add_argument(
        "-l",
        dest="layer",
        metavar="LAYER",
        help="number of point attribute layer",
        type=int,
        default=0,
    )

    group = parser.add_mutually_exclusive_group()
    group.add_argument(
        "-w", "--write", help="write the points to a JSON file", type=str
    )
    group.add_argument(
        "-p",
        "--plot",
        help="plot the points (in 2D using plotly)",
        action="store_true",
    )

    parser.add_argument(
        "-z", "--allow-empty", help="allow empty graphs", action="store_true"
    )
    args = parser.parse_args()

    df = gg2pd_points(
        map_name=args.map,
        mapset=args.mapset,
        layer=args.layer,
        allow_empty=args.allow_empty,
    )

    if args.write:
        df.to_json(args.write)
    elif args.plot:
        import plotly.express as px

        fig = px.scatter(
            df,
            x="_%s" % _ATTR_POINT_COORDS_X,
            y="_%s" % _ATTR_POINT_COORDS_Y,
            hover_data=df.columns,
        )
        fig.update_yaxes(scaleanchor="x", scaleratio=1)
        fig.show()
    else:
        print(df)
