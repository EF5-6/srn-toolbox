#!/usr/bin/env python3
# pylama:ignore=D203,D213

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Implements a class to manage the session of a GRASS script."""

import atexit
import os

from contextlib import contextmanager

from grass.pygrass.messages import Messenger
from grass.pygrass.modules.shortcuts import general as g


class GrassScriptSession:
    """Maintain temporary maps and settings of a GRASS script.

    This also manages a :class:`~grass.pygrass.messages.Messenger` instance.
    """

    def __init__(self, prefix="gss", save_region=False):
        """Initialize a GRASS script session.

        :param str prefix:
            Prefix to use for temporary map names.

        :param bool save_region:
            Whether to save the current region and restore it on cleanup.
        """
        self._prefix = prefix
        self._pid = os.getpid()
        self._saved_region = None
        self._temp_rast_maps = set()
        self._temp_vect_maps = set()
        self._temp_regions = set()
        self._messenger = Messenger(raise_on_error=True)

        if save_region:
            self._save_initial_region()

        atexit.register(self._cleanup)

    def _make_temp_name(self, basename):
        return "%s_%d_%s" % (self._prefix, self._pid, basename)

    def _save_initial_region(self):
        """Save region under a temporary name to restore it on cleanup."""
        self._saved_region = self.region("_saved")
        g.region(save=self._saved_region, quiet=True)

    def _cleanup(self):
        """Restore a saved region and delete temporary maps."""
        if self._saved_region:
            g.region(region=self._saved_region, quiet=True)

        if self._temp_rast_maps:
            g.remove(
                flags="f",
                quiet=True,
                type="raster",
                name=",".join(self._temp_rast_maps),
            )

        if self._temp_vect_maps:
            g.remove(
                flags="f",
                quiet=True,
                type="vector",
                name=",".join(self._temp_vect_maps),
            )

        if self._temp_regions:
            g.remove(
                flags="f",
                quiet=True,
                type="region",
                name=",".join(self._temp_regions),
            )

    def raster(self, basename, is_new=None):
        """Create and track a temporary raster map name."""
        name = self._make_temp_name(basename)

        if is_new is not None:
            if is_new and name in self._temp_rast_maps:
                raise ValueError(f"Raster map '{name}' exists.")
            elif not is_new and name not in self._temp_rast_maps:
                raise LookupError(f"Raster map '{name}' does not exist.")

        self._temp_rast_maps.add(name)
        return name

    def vector(self, basename, is_new=None):
        """Create and track a temporary vector map name."""
        name = self._make_temp_name(basename)

        if is_new is not None:
            if is_new and name in self._temp_vect_maps:
                raise ValueError(f"Vector map '{name}' exists.")
            elif not is_new and name not in self._temp_vect_maps:
                raise LookupError(f"Vector map '{name}' does not exist.")

        self._temp_vect_maps.add(name)
        return name

    def region(self, basename, is_new=None):
        """Create and track a temporary region name."""
        name = self._make_temp_name(basename)

        if is_new is not None:
            if is_new and name in self._temp_regions:
                raise ValueError(f"Saved region '{name}' exists.")
            elif not is_new and name not in self._temp_regions:
                raise LookupError(f"Saved region '{name}' does not exist.")

        self._temp_regions.add(name)
        return name

    @contextmanager
    def vector_inplace(self, basename):
        """Mimic an in-place operation on vector maps."""
        old = self.vector(basename, is_new=False)
        new = self.vector(basename + "__new__", is_new=True)

        yield old, new

        g.remove(type="vector", name=old, quiet=True, flags="f")
        g.rename(vector=(new, old), quiet=True)
        self._temp_vect_maps.remove(new)

    @property
    def msg(self):
        """Get the :class:`~grass.pygrass.messages.Messenger` instance."""
        return self._messenger

    @property
    def message(self):
        """Return the session's normal message logging function."""
        return self._messenger.message

    @property
    def verbose(self):
        """Return the session's verbose message logging function."""
        return self._messenger.vebrose

    @property
    def important(self):
        """Return the session's important message logging function."""
        return self._messenger.important

    @property
    def warning(self):
        """Return the session's warning message logging function."""
        return self._messenger.warning

    @property
    def error(self):
        """Return the session's error message logging function."""
        return self._messenger.error

    @property
    def fatal(self):
        """Return the session's fatal message logging function."""
        return self._messenger.fatal

    @property
    def debug(self):
        """Return the session's debug message logging function."""
        return self._messenger.debug

    @property
    def percent(self):
        """Return the session's percentage logging function."""
        return self._messenger.percent
