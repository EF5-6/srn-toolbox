#!/bin/sh

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

# #############################################################################
# Edit the following settings to match your research region.
# -----------------------------------------------------------------------------

# INPUT SETTINGS
# ---------------

INPUT_REGION="sardinia"

INPUT_RASTER_ELEVATION="elevation_50m_lanczos"

INPUT_VECTOR_WATER="openwater"

# Lines representing the reference road network.
INPUT_VECTOR_ROADS="mastino2005_fig37_roads"

# Points including "sites" and points of evidence, here coined "stations".
INPUT_VECTOR_PLACES="mastino2005_fig37_places"
SQL_PLACE_IS_SITE="itinerarium_antonini and selected"
SQL_PLACE_IS_EVIDENCE="itinerarium_antonini and not selected"

# Points to use as (additional) evidence, here referred to as "milestones".
INPUT_VECTOR_MILESTONES="mastino2005_fig37_milestones"

# OUTPUT SETTINGS
# ---------------

OUTPUT_MAPSET="mastino2005_fig37_detailed"

OUTPUT_RASTER_SLOPE="slope"
OUTPUT_RASTER_LANDSLOPE="landslope"
OUTPUT_RASTER_WATER="openwater"
OUTPUT_RASTER_COST="toblers_hiking"

OUTPUT_VECTOR_SITES="sites"
OUTPUT_VECTOR_STATIONS="stations"
OUTPUT_VECTOR_EVIDENCE="evidence"  # stations and milestones
OUTPUT_VECTOR_TERRAIN="terrain"
OUTPUT_VECTOR_REFERENCE="reference"
OUTPUT_VECTOR_REFERENCE_SITES="reference_sites"

OUTPUT_PYTHON_DIRECTORY="${OUTPUT_MAPSET}"
OUTPUT_PYTHON_EVIDENCE="${OUTPUT_PYTHON_DIRECTORY}/evidence.json"
OUTPUT_PYTHON_TERRAIN="${OUTPUT_PYTHON_DIRECTORY}/terrain.pickle"
OUTPUT_PYTHON_REFERENCE="${OUTPUT_PYTHON_DIRECTORY}/reference.pickle"

# OTHER SETTINGS

MAX_MEMORY=8000
WATER_SLOPE_DEGREES=30
SITE_SNAP=500
ROAD_SNAP=500
NOISE=0.1

# #############################################################################

set -e

# Setup mapset.
echo -e "INITIALIZING MAPSET\n"
g.mapset -c "${OUTPUT_MAPSET}"
g.region "${INPUT_REGION}"

# Obtain sites and evidence from places.
echo -e "\nOBTAINING SITES AND STATIONS FROM PLACES\n"
v.extract \
    input="${INPUT_VECTOR_PLACES}" \
    output="${OUTPUT_VECTOR_SITES}" \
    where="${SQL_PLACE_IS_SITE}" \
    --overwrite
v.extract \
    input="${INPUT_VECTOR_PLACES}" \
    output="${OUTPUT_VECTOR_STATIONS}" \
    where="${SQL_PLACE_IS_EVIDENCE}" \
    --overwrite

# Merge evidence locations.
echo -e "\MERGING EVIDENCE LOCATIONS\n"
v.patch \
    input="${OUTPUT_VECTOR_STATIONS},${INPUT_VECTOR_MILESTONES}" \
    output="${OUTPUT_VECTOR_EVIDENCE}" \
    --overwrite

# Create cost surface.
echo -e "\nCOMPUTING COST SURFACE\n"
r.slope.aspect \
    "elevation=${INPUT_RASTER_ELEVATION}" \
    "slope=${OUTPUT_RASTER_SLOPE}" \
    --overwrite
v.to.rast \
    "input=${INPUT_VECTOR_WATER}" \
    type=area \
    use=val \
    "output=${OUTPUT_RASTER_WATER}" \
    "memory=${MAX_MEMORY}" \
    --overwrite
r.mapcalc \
    "${OUTPUT_RASTER_LANDSLOPE} = max(${OUTPUT_RASTER_SLOPE}, ${WATER_SLOPE_DEGREES} * !isnull(${OUTPUT_RASTER_WATER}))" \
    --overwrite
r.mapcalc \
    "${OUTPUT_RASTER_COST} = 0.6 * 2.7182818 ^ (3.5 * abs(tan(${OUTPUT_RASTER_LANDSLOPE}) + 0.05))" \
    --overwrite

# Load historical road network.
echo -e "\nLOADING REFERENCE NETWORK\n"
./v.net.trivialize.py \
    -c \
    sites="${OUTPUT_VECTOR_SITES}" \
    roads="${INPUT_VECTOR_ROADS}" \
    costs="${OUTPUT_RASTER_COST}" \
    output="${OUTPUT_VECTOR_REFERENCE}" \
    output_sites="${OUTPUT_VECTOR_REFERENCE_SITES}" \
    sitesnap="${SITE_SNAP}" \
    roadsnap="${ROAD_SNAP}" \
    --overwrite

# Create junction network.
echo -e "\nCOMPUTING TERRAIN NETWORK\n"
./v.net.junctions.py \
    -k \
    sites="${OUTPUT_VECTOR_REFERENCE_SITES}" \
    extra="${OUTPUT_VECTOR_STATIONS}" \
    costs="${OUTPUT_RASTER_COST}" \
    output="${OUTPUT_VECTOR_TERRAIN}" \
    noise="${NOISE}" \
    memory="${MAX_MEMORY}" \
    --overwrite \
    --verbose

# Export networks and evidence to Python.
echo -e "\nEXPORTING TO PYTHON\n"
mkdir -p "${OUTPUT_PYTHON_DIRECTORY}"
echo "Exporting reference network..."
./grassgis2networkx.py \
    "${OUTPUT_VECTOR_REFERENCE}" \
    -e 1 \
    -n 2 \
    -w "${OUTPUT_PYTHON_REFERENCE}"
echo "Exporting terrain network..."
./grassgis2networkx.py \
    "${OUTPUT_VECTOR_TERRAIN}" \
    -e 1 \
    -n 2 \
    -w "${OUTPUT_PYTHON_TERRAIN}"
echo "Exporting evidence..."
./grassgis2pandas.py \
    "${OUTPUT_VECTOR_EVIDENCE}" \
    -w "${OUTPUT_PYTHON_EVIDENCE}"
