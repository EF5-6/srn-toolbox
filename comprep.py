#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203,E741

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Generate compositional representations of road construction orders."""

import pickle
from functools import lru_cache

import composition_stats as cs
import numpy as np
import picos as pc

EPSILON = 1e-8


@lru_cache()
def nudge_vector(n):
    """Generate a strictly increasing compos. vector of very close elements."""
    w = cs.closure(np.arange(1, n + 1, dtype=float))

    while True:
        u = cs.power(w, 0.5)

        if len(set(u)) < n:
            assert len(set(w)) == n
            return w
        else:
            w = u


def increasing(v):
    """Report whether a vector is strictly increasing."""
    return np.all(v[1:] > v[:-1])


def decreasing(v):
    """Report whether a vector is strictly decreasing."""
    return np.all(v[1:] < v[:-1])


def aitchison_distance(comp1, comp2):
    """Compute the Aitchison distance."""
    difference = cs.perturb_inv(comp1, comp2)
    return cs.inner(difference, difference) ** 0.5


class CompositionalRepresentation:
    """Defines a compositional representation of road construction orders."""

    # -------------------------------------------------------------------------
    # Initialization and special methods and properties.
    # -------------------------------------------------------------------------

    def __init__(self, weights):
        """Initialize a compositional representation for given weights."""
        if np.any(weights <= 0):
            raise ValueError("Some weight is nonpositive.")
        elif np.any(np.isinf(weights)):
            raise ValueError("Some weight is infinite.")
        elif decreasing(weights):
            self._decreasing = True
        elif increasing(weights):
            self._decreasing = False
        else:
            raise ValueError("Weights are not strictly monotonic.")

        self.weights = cs.closure(weights)
        self._origin = "from a weight vector"
        self._from_file = False
        self._params = None

    def __len__(self):
        """Vector length of the representation (ILR has one dimension less)."""
        return len(self.weights)

    def __str__(self):
        """Get a basic string description."""
        return f"{len(self)}D CR {self._origin}"

    @property
    def param_str(self):
        """Get a string listing the fit parameters."""
        if self._params:
            k, h, beta, lbd, delta, gamma = self._params

            return (
                f"k={k} h={h} b={beta:.1e} l={lbd:.3f} d={delta:.1e} "
                f"g={gamma:.3f}"
            )
        else:
            return "unknown"

    # -------------------------------------------------------------------------
    # Methods to compute the weights.
    # -------------------------------------------------------------------------

    @classmethod
    def from_stats_file(cls, filename, verbose=False, return_params=False):
        """Like :meth:`from_stats_vector` but load the vector from a file."""
        if verbose:
            print(f"# Loading transposition statistics from {filename}...")

        with open(filename, "rb") as fp:
            y = pickle.load(fp)

        if not isinstance(y, np.ndarray):
            raise TypeError("File did not contain a NumPy array.")

        if len(y.shape) == 2:
            y = np.diagonal(y, 1)

        if len(y.shape) != 1:
            raise ValueError("NumPy array is not one- or two-dimensional.")

        result = cls.from_stats_vector(
            adj_trans_mean_rel_cost_change=y,
            verbose=verbose,
            return_params=return_params,
        )

        if return_params:
            return result
        else:
            assert isinstance(result, cls)
            result._origin = f"fitted to {filename}"
            result._from_file = True
            return result

    @staticmethod
    def fit_exp_pow(y, h):
        r"""Fit an exponential to 1..h -> y[:h], a power to h+1..k -> y[h:].

        :returns:
            A pair :math:`v, (k, h, \beta, \lambda, \gamma, \delta)` such that
            :math:`\beta e^{\lambda x[:h]} \approx y[:h]` and :math:`\delta
            x[h:]^{\gamma} \approx y[h:]` and :math:`\beta e^{\lambda x[h +
            \frac{1}{2}]} = \delta x[h + \frac{1}{2}]^{\gamma}`. The value
            :math:`v` is the sum of squared errors for both parts.

        :rtype:
            tuple(float, tuple(int, int, float, float, float, float))
        """
        k = len(y) + 1
        x = np.arange(1, k)

        xe, ye = x[:h], y[:h]  # Part where we expect an exponential regime.
        xp, yp = x[h:], y[h:]  # Part where we expect a power regime.

        # We fit in log/log-log space, so sort out zero measurements.
        ve, vp = np.nonzero(ye), np.nonzero(yp)

        # Check if enough measurements remain.
        if np.sum(ve) < 2:
            raise RuntimeError(
                "Not enough nonzero samples for the exponential regime."
            )
        elif np.sum(vp) < 2:
            raise RuntimeError(
                "Not enough nonzero samples for the power regime."
            )

        # Simultaneously fit the exponential and the power under the constraint
        # that they touch at the boundary of their regimes.
        x_0 = pc.Constant("x_0", h + 0.5)
        x_e = pc.Constant("x_e", xe[ve])
        log_y_e = pc.Constant("log(y_e)", np.log(ye[ve]))
        log_x_p = pc.Constant("log(x_p)", np.log(xp[vp]))
        log_y_p = pc.Constant("log(y_p)", np.log(yp[vp]))
        l = pc.RealVariable("lambda", upper=-EPSILON)
        g = pc.RealVariable("gamma", upper=-EPSILON)
        log_b = pc.RealVariable("log(beta)")
        log_d = pc.RealVariable("log(delta)")
        pc.minimize(
            abs((x_e & 1) * (l // log_b) - log_y_e)
            + abs((log_x_p & 1) * (g // log_d) - log_y_p),
            [x_0 * l + log_b == pc.log(x_0) * g + log_d],
            solver="cvxopt",
        )

        # Extract solution values.
        lbd, gamma, log_beta, log_delta = (l // g // log_b // log_d).np
        beta, delta = np.exp(log_beta), np.exp(log_delta)
        params = k, h, beta, lbd, delta, gamma

        # Evalute the fit in linear space.
        fe = beta * np.exp(lbd * xe)
        fp = delta * xp**gamma
        fit = np.concatenate([fe, fp])
        value = np.linalg.norm(fit - y)

        return value, params

    @classmethod
    def from_stats_vector(
        cls,
        adj_trans_mean_rel_cost_change,
        *,
        breakpoint=None,
        verbose=False,
        return_params=False,
    ):
        r"""Generate from a SRN-specific statistics vector.

        Weights are chosen such that the Aitchison distance for adjacent
        transposition is proportional to a continuous curve that is fit to the
        given vector. This curve describes exponential decay for a first part
        of the sequence and follows a power law for its remainder.

        :param numpy.ndarray adj_trans_mean_rel_cost_change:
            A NumPy vector :math:`x \in \mathbb{R}^{k - 1}` where :math:`k` is
            the number of connections and where :math:`x_i` is a measure of
            change in the network structure when :math:`\pi_i` and
            :math:`\pi_{i + 1}` are swapped in a connection sequence
            :math:`\pi` drawn uniformly at random from :math:`S(K)`, :math:`K`
            being the set of connections.

        :param int or None breakpoint:
            Breakpoint for the split regime. If :obj:`None`, try all possible
            and select the best fit.
        """
        y = adj_trans_mean_rel_cost_change
        k = len(y) + 1

        if not isinstance(y, np.ndarray):
            raise TypeError("Argument must be a NumPy array.")

        if len(y.shape) != 1:
            raise ValueError("Argument must be one-dimensional.")

        if verbose:
            print("# Computing compositional representation parameters...")

        best_value = float("inf")
        best_params = None

        if breakpoint:
            h = breakpoint
            best_value, best_params = cls.fit_exp_pow(y, h)
        else:
            # TODO: Parallelize this.
            for h in range(2, k - 3):
                try:
                    value, params = cls.fit_exp_pow(y, h)
                except RuntimeError:
                    pass
                except ValueError as error:
                    if str(error) == "math domain error":
                        # HACK: Ignore an occasional CVXOPT numerics issue.
                        if verbose:
                            print(
                                f"# Skipping breakpoint h={h} "
                                f"due to numerical issues."
                            )
                else:
                    if value < best_value:
                        best_value = value
                        best_params = params

            if not params:
                raise RuntimeError("Not enough samples to fit.")

        params = best_params
        k, h, beta, lbd, delta, gamma = params

        if verbose:
            print(
                f"# Parameters are k={k}, h={h}, "
                f"beta={beta:.1e}, lambda={lbd:.3f}, "
                f"delta={delta:.1e}, gamma={gamma:.3f}."
            )

        if return_params:
            return params
        else:
            comprep = cls.from_exp_pow_params(*params)
            comprep._origin = "fitted to a stats vector"
            return comprep

    @classmethod
    def from_exp_pow_params(
        cls, k, h, beta, lbd, delta, gamma, *, return_fit=False
    ):
        """Generate from parameters estimated by :meth:`from_stats_vector`."""
        x = np.arange(1, k)

        xe = x[:h]
        xp = x[h:]

        fe = beta * np.exp(lbd * xe)
        fp = delta * xp**gamma

        fit = np.concatenate([fe, fp])

        if return_fit:
            return fit
        else:
            comprep = cls.from_aitchison_distances(fit)
            comprep._origin = "from given fit parameters"
            comprep._params = (k, h, beta, lbd, delta, gamma)
            return comprep

    @classmethod
    def from_aitchison_distances(cls, distances, normalize=True):
        """Generate from target Aitchison distances for adj. transposition."""
        # TODO: Optimize normalization with respect to numeric considerations.
        if normalize:
            distances = distances / sum(distances)

        if increasing(distances):
            # TODO: Check if this variant behaves any differently.
            raise NotImplementedError(
                "Only decreasing distances are supported at this point."
            )
        elif decreasing(distances):
            k = len(distances) + 1
            w = np.zeros(k)

            w[0] = 1.0
            for i in range(1, k):
                w[i] = np.exp(distances[i - 1] / np.sqrt(2)) * w[i - 1]

            comprep = cls(w)
            comprep._origin = "from Aitchison distances"
            return comprep
        else:
            raise ValueError("Distances are not monotonic.")

    # -------------------------------------------------------------------------
    # Methods to transform plans.
    # -------------------------------------------------------------------------

    def compositional(self, plan):
        """Compute the compositional representation of a plan's order."""
        if len(plan) != len(self):
            raise ValueError(
                f"Order has length {len(plan)}, "
                f"weights have length {len(self.weights)}."
            )

        return self.weights[list(plan.positions)]

    def clr(self, plan):
        """Center log ratio transformation of :meth:`compositional`."""
        return cs.clr(self.compositional(plan))

    def ilr(self, plan):
        """Isometric log ratio transformation of :meth:`compositional`."""
        return cs.ilr(self.compositional(plan))

    def plan_from_composition(self, v, alpha, simulation=None):
        """Construct a road construction plan from a compositional vector."""
        from population import Plan

        if len(v) != len(self):
            raise ValueError(
                "Compositional input vector must have same dimensionality "
                "as the compositional representation."
            )

        order = sorted(
            range(len(self)), key=v.__getitem__, reverse=self._decreasing
        )

        return Plan(order, alpha, simulation)

    def plan_from_crl(self, v, alpha, simulation=None):
        """Construct a road construction plan from a CLR vector."""
        if len(v) != len(self):
            raise ValueError(
                "CLR input vector must have same dimensionality "
                "as the compositional representation."
            )

        # TODO: Verify that CLR is monotonic, then remove call to clr_inv.
        return self.plan_from_composition(cs.clr_inv(v), alpha, simulation)

    def plan_from_irl(self, v, alpha, simulation=None):
        """Construct a road construction plan from an ILR vector."""
        if len(v) != len(self) - 1:
            raise ValueError(
                "ILR input vector must have one dimension less "
                "than the compositional representation."
            )

        return self.plan_from_composition(cs.ilr_inv(v), alpha, simulation)


@lru_cache()
def fallback_comprep(n):
    """Generate a fallback compositional representation."""
    w = np.arange(1, n + 1, dtype=float)
    comprep = CompositionalRepresentation(w)
    comprep._origin = "with fallback weights"
    return comprep
