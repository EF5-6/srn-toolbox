#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Find good sequential road networks using distribution estimates."""

import argparse
import datetime
import logging
import os
import re
from abc import ABC, abstractmethod
from pathlib import Path

import numpy as np
import scipy.stats

from comprep import CompositionalRepresentation
from helper import Timer, load_network, load_points
from population import Population
from simulation import (
    TARGET_TYPE_COST,
    TARGET_TYPE_EVIDENCE,
    TARGET_TYPE_REFERENCE,
    Simulation,
)


class EDA_Model(ABC):
    """An abstract base class for distributions estimated by :class:`EDA`."""

    # -------------------------------------------------------------------------
    # Interface-provided methods.
    # -------------------------------------------------------------------------

    def __init__(self, eda):
        """Initialize the distribution."""
        self._eda = eda
        self._parameterized = False
        self._init()

    @property
    def dim(self):
        """Report the dimensionality of the search space."""
        return self._eda.search_space_dim

    def parameterize(self, samples):
        """(Re-)parameterize the distribution from selected samples.

        :param numpy.ndarray samples:
            A NumPy 2D-array whose columns are the samples to estimate the
            parameters from.
        """
        assert samples.shape[0] == self.dim

        try:
            self._parameterize(samples)
        except Exception as error:
            rel = samples.shape[1] / samples.shape[0]
            raise RuntimeError(
                f"Estimating the distribution parameters failed. "
                f"There are {rel:.2f} times as many samples as dimensions."
            ) from error

        self._parameterized = True

    def sample(self, n):
        """Sample the distribution :math:`n` times.

        If the distribution was not yet parameterized, draw from the uniform
        distribution in the solution space.

        :returns numpy.ndarray:
            A NumPy 2D-array whose columns are the samples drawn.
        """
        if self._parameterized:
            try:
                return self._sample(n)
            except Exception as error:
                raise RuntimeError(
                    "Sampling from the estimated distribution failed."
                ) from error
        else:
            return self._eda.sample_uniform(n)

    # -------------------------------------------------------------------------
    # Abstract and default-implementation methods.
    # -------------------------------------------------------------------------

    def _init(self):
        pass

    @abstractmethod
    def _parameterize(self, samples):
        pass

    @abstractmethod
    def _sample(self, n):
        pass

    @property
    def stats_string(self):
        """Report a compact statistics string."""
        return ""


class EDA_ThresheldModel(EDA_Model):
    """An abstract base class for thresheld-estimate distributions."""

    # -------------------------------------------------------------------------
    # Interface-provided methods.
    # -------------------------------------------------------------------------

    def __init__(self, *args, **kwargs):  # noqa
        EDA_Model.__init__(self, *args, **kwargs)

        self._gamma = 1.0
        self._threshold = None

    def _update_threshold(self):
        if self._parameterized:
            if self._adaptive:
                # Obtain a handle to previous generations.
                generations = self._eda._population.generations
                assert len(generations) >= 2

                # Adjust the convergence rate based on whether the last
                # generation found a better solution than the one before.
                if generations[-1].best_cost < generations[-2].best_cost:
                    self._gamma *= 0.95  # Delay convergence.
                else:
                    self._gamma *= 1.05  # Accelerate convergence.

            d = self._initial
            g = self._gamma
            te = self._eda.total_evaluations
            de = self._eda._done_evals

            self._threshold = d * ((te - de) / te) ** g
        else:
            self._threshold = self._initial = self._initial_threshold()

    # -------------------------------------------------------------------------
    # Abstract and default-implementation methods.
    # -------------------------------------------------------------------------

    @abstractmethod
    def _initial_threshold(self):
        """Compute initial threshold. Called from :meth:`_update_threshold`."""
        pass

    @property
    def _adaptive(self):
        """Whether the convergence rate should be auto-adjusted."""
        return True

    @property
    def stats_string(self):  # noqa
        return f"g={self._gamma:.2f} t={self._threshold:.2e}"


class MultivariateNormal(EDA_Model):
    """A multivariate normal distribution estimate."""

    def _parameterize(self, samples):
        self._mean = np.mean(samples, axis=1)
        self._cov = np.cov(samples)

    def _sample(self, n):
        return np.random.multivariate_normal(self._mean, self._cov, size=n).T


class ThresheldMultivariateNormal(EDA_ThresheldModel):
    """A multivariate normal distribution estimate with thresheld convergence.

    This and :class:`EDA_ThresheldModel` are based on the paper "Estimation
    Multivariate Normal Algorithm with Thresheld Convergence" (Tamayo-Vera,
    Bolufé-Röhler, Chen 2016: http://dx.doi.org/10.1109/CEC.2016.7744223).
    """

    def _initial_threshold(self):
        return np.linalg.norm(self._cov)

    def _parameterize(self, samples):
        MultivariateNormal._parameterize(self, samples)

        self._update_threshold()

        # Rescale the covariance matrix based on the threshold.
        self._cov = self._threshold * (self._cov / np.linalg.norm(self._cov))

    def _sample(self, n):
        return MultivariateNormal._sample(self, n)


class GaussianKDE(EDA_Model):
    """A Gaussian kernel density estimator."""

    def _parameterize(self, samples):
        self._dist = scipy.stats.gaussian_kde(samples)

    def _sample(self, n):
        return self._dist.resample(n)


class ThresheldGaussianKDE(EDA_ThresheldModel):
    """A Gaussian kernel density estimator with thresheld convergence."""

    def _initial_threshold(self):
        return 1.0

    def _parameterize(self, samples):
        self._update_threshold()

        # Use the threshold as the bandwith.
        self._dist = scipy.stats.gaussian_kde(samples, self._threshold)

    def _sample(self, n):
        return self._dist.resample(n)


class VonMisesFisher(EDA_Model):
    """Von Mises-Fisher distribution estimate."""

    def _init(self):
        logger = logging.getLogger()
        logger.disabled = True

        import geomstats

        logger.disabled = False

        assert (
            self._eda._ilr
        ), f"{self.__class__.__name__} requires an ILR representation."

        self._sphere = geomstats.geometry.hypersphere.Hypersphere(self.dim - 1)
        self._last_mu = None

    def _parameterize(self, samples):
        # Samples are of same magnitude but not necessarily normalized.
        S = samples / np.linalg.norm(samples, axis=0)

        # Compute the mean normalized sample and its norm.
        mean = np.mean(S, axis=1)
        norm = np.linalg.norm(mean)
        sqnorm = norm**2

        # Make an (approximate) maximum likelihood estimate of the parameters.
        # High-dimensional estimate for kappa first described in "Clustering on
        # the Unit Hypersphere using von Mises-Fisher Distributions." (Banerjee
        # et al. 2005; J. Mach. Learn. Res. 6).
        self._mu = mean / norm
        self._kappa = norm * (self.dim - sqnorm) / (1 - sqnorm)

    def _sample(self, n):
        return self._sphere.random_von_mises_fisher(self._mu, self._kappa, n).T

    @property
    def stats_string(self):  # noqa
        if self._last_mu is None:
            mu_change = float("nan")
        else:
            mu_change = np.linalg.norm(self._mu - self._last_mu)

        self._last_mu = self._mu

        return f"k={self._kappa:.2e} dm={mu_change:.2e}"


class EMMFA(EDA_Model):
    """Estimation of Mixture of von Mises-Fisher Distributions Algorithm."""

    def _init(self):
        import emmfa

        assert (
            self._eda._ilr
        ), f"{self.__class__.__name__} requires an ILR representation."

        self._external = emmfa.EMMFA(
            sphere_dim=self.dim - 1,
            function=self._eda.evaluate_for_external,
            population_factor=(
                self._eda._population_size / self._eda.search_space_dim
            ),
            survival_factor=(
                self._eda._selection_size / self._eda._population_size
            ),
        )

        self._converged = False

    def _parameterize(self, samples):
        assert False

    def _sample(self, n):
        assert False

    def external_step(self):  # noqa
        if self._converged:
            return

        try:
            self._external.step()
        except self._external.Convergence:
            self._converged = True

    @property
    def stats_string(self):  # noqa
        return f"C {len(self._external._clusters):03d}"


# TODO: Save console output/header also to output directory.
class EDA:
    """An estimation of distribution algorithm for sequential road networks."""

    def __init__(
        self,
        simulation,
        alpha,
        *,
        distribution=ThresheldMultivariateNormal,
        use_ilr=True,
        population_factor=50,
        survival=0.3,
        generations=50,
        save_progress=True,
        debug_progress=False,
    ):
        """Initialize the algorithm.

        :param Simulation simulation:
            Simulation context.

        :param float alpha:
            The network formation parameter alpha.

        :param int population_factor:
            The population size is set to this factor times the search space
            dimension.

        :param float survival:
            Fraction of the population to re-estimate the distribution with.

        :param int generations:
            Number of generations to produce. This is the stopping criterion.

        :param EDA_Model distribution:
            The type of distribution to estimate.

        :param bool use_ilr:
            Whether to use ILR instead of CLR as the isometric transformation.

        :param bool save_progress:
            Whether to store the sequence of best plans found in a directory.

        :param bool debug_progress:
            Whether to store the sequence of populations.
        """
        self._ilr = use_ilr
        self._simulation = simulation
        self._alpha = alpha
        self._population_size = population_factor * self.search_space_dim
        self._selection_size = round(survival * self._population_size)
        self._generations = generations
        self._distribution = distribution(self)

        if self._simulation._target_type == TARGET_TYPE_COST:
            target_arg = "l"
            target_name = "Minimize costs"
        elif self._simulation._target_type == TARGET_TYPE_REFERENCE:
            target_arg = "r"
            target_name = (
                f"Fit reference network "
                f"{self._simulation._target.graph['file']}"
            )
        elif self._simulation._target_type == TARGET_TYPE_EVIDENCE:
            target_arg = "e"
            target_name = (
                f"Fit to evidence " f"{self._simulation._target.attrs['file']}"
            )
        else:
            assert False, "Unexpected target type."

        self._short_string = (
            f"{'i' if self._ilr else 'c'}"
            f"{re.sub('[a-z]', '', self._distribution.__class__.__name__)}"
            f"{target_arg}"
            f"a{self._alpha}"
            f"p{population_factor}"
            f"s{survival}"
            f"g{self._generations}"
            f"{'f' if self._simulation._comprep._from_file else ''}"
        )

        progress_dir = debug_file = None
        if save_progress or debug_progress:
            t = datetime.datetime.now().isoformat(sep="_", timespec="seconds")
            suffix = f"{self._short_string}_{t}_{os.uname().nodename}"

            if save_progress:
                progress_dir = Path(f".eda_{suffix}")

            if debug_progress:
                debug_file = Path(f".eda_{suffix}.pickle")

        print(
            "# " + "-" * 70 + f"\n"
            f"# Start time : {datetime.datetime.now()}\n"
            f"# Short name : {self._short_string}\n"
            f"# Transform  : {'ILR' if self._ilr else 'CLR'}\n"
            f"# Model      : {distribution.__name__}\n"
            f"# Terrain    :"
            f" {self._simulation._terrain_network.graph['file']}\n"
            f"# Target     : {target_name}\n"
            f"# Alpha      : {alpha:.4f}\n"
            f"# Population : {self._population_size}"
            f" ({population_factor} × {self.search_space_dim})\n"
            f"# Selection  : {self._selection_size}"
            f" ({survival:.2f} × {self._population_size})\n"
            f"# Generations: {generations}\n"
            f"# Comprep    : {self._simulation._comprep}\n"
            f"# Comprep fit: {self._simulation._comprep.param_str}\n"
            f"# Performance: {self._simulation.num_connections} connections, "
            f"{self.total_evaluations:.1e} evaluations, "
            f"{os.cpu_count()} CPUs\n"
            f"# Saving to  : {progress_dir}\n"
            f"# " + "-" * 70
        )

        self._population = Population(
            simulation, save_best_to=progress_dir, save_as=debug_file
        )
        self._timer = Timer()

    def __str__(self):
        """Return a string description of the algorithm parameters."""
        return self._short_string

    @property
    def total_evaluations(self):
        """Report total number of network evaluations."""
        return self._population_size * self._generations

    @property
    def search_space_dim(self):
        """Report the dimensionality of the search space."""
        if self._ilr:
            return self._simulation.num_connections - 1
        else:
            return self._simulation.num_connections

    def sample_uniform(self, n):
        """Sample in the search space the solution space uniformly."""
        return np.random.normal(size=(self.search_space_dim, n))

    def sample_plans(self, n):
        """Sample plans from the current distribution estimate."""
        weights = self._distribution.sample(n)

        assert weights.shape == (self.search_space_dim, n)

        if self._ilr:
            method = self._simulation._comprep.plan_from_irl
        else:
            method = self._simulation._comprep.plan_from_crl

        return tuple(
            method(weights[:, i], self._alpha, self._simulation)
            for i in range(n)
        )

    @staticmethod
    def _seconds2str(s):
        td = datetime.timedelta(seconds=int(s))

        if td.days:
            days_part = f"{td.days};"
            td = datetime.timedelta(seconds=td.seconds)
            assert not td.days
        else:
            days_part = ""

        time_part = f"{td}"
        if len(time_part) == 7:
            time_part = f"0{time_part}"

        return f"{days_part}{time_part}"

    def evaluate_for_external(self, samples):
        """Allow an external EDA to evaluate a sample matrix."""
        # NOTE: Assume numpy axis conventions.
        assert samples.shape[1] == self.search_space_dim

        if self._ilr:
            method = self._simulation._comprep.plan_from_irl
        else:
            method = self._simulation._comprep.plan_from_crl

        self._population += tuple(
            method(samples[i], self._alpha, self._simulation)
            for i in range(samples.shape[0])
        )

        generation = self._population.last

        return np.array(generation.costs)

    def run(self):
        """Run the algorithm."""
        self._timer.reset()

        n = self._population_size
        k = self._selection_size

        for round_ in range(self._generations):
            self._done_evals = round_ * n  # Used by thresheld approaches.

            with self._timer.measure(n):
                if hasattr(self._distribution, "external_step"):  # HACK
                    # Let an external evolutionary algorithm perform one step.
                    # By using evaluate_for_external, it will add a generation
                    # to self._population just like below.
                    self._distribution.external_step()

                    generation = self._population.last
                else:
                    # Sample new plans.
                    plans = self.sample_plans(n)

                    # Evaluate them.
                    self._population += plans
                    generation = self._population.last

                    # Transform the best plans to vectors in the search space
                    # and assemble a sample matrix from them.
                    samples = np.c_[
                        tuple(
                            plan.ilr if self._ilr else plan.clr
                            for plan in generation.sorted_plans[:k]
                        )
                    ]
                    assert samples.shape == (self.search_space_dim, k)

                    # Reparameterize the distribution.
                    self._distribution.parameterize(samples)

            # Print statistics.
            seconds_remaining = (
                self.total_evaluations - (round_ + 1) * n
            ) * self._timer.time_per_thing
            extra = self._distribution.stats_string
            extra = f"  {extra}" if extra else ""
            print(
                f"{round_ + 1:03d}  "
                f"{self._seconds2str(self._timer.elapsed)}  "
                f"{self._seconds2str(seconds_remaining)}  "
                f"{1 / self._timer.time_per_thing:4.2f}/s  "
                f"≥ {self._population.best_cost:10.04e}  "
                f"↓ {generation.best_cost:10.04e}  "
                f"Ø {generation.mean_cost:10.04e}  "
                f"± {generation.std:8.02e}"
                f"{extra}"
            )


if __name__ == "__main__":
    defaults = EDA.__init__.__kwdefaults__
    parser = argparse.ArgumentParser(
        description="Search good sequential road networks.",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-i",
        "--ilr",
        dest="use_ilr",
        help="use the isometric log-ration transform",
        action="store_true",
    )
    group.add_argument(
        "-c",
        "--clr",
        dest="use_ilr",
        help="use the center log-ration transform",
        action="store_false",
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-1",
        "--emna",
        dest="method",
        help="estimate a multivariate normal",
        action="store_const",
        const=1,
        default=argparse.SUPPRESS,
    )
    group.add_argument(
        "-2",
        "--emna-tc",
        dest="method",
        help="estimate a multiv. norm. w/ thresheld convergence",
        action="store_const",
        const=2,
        default=argparse.SUPPRESS,
    )
    group.add_argument(
        "-3",
        "--gkde",
        dest="method",
        help="use a Gaussian kernel density estimate",
        action="store_const",
        const=3,
        default=argparse.SUPPRESS,
    )
    group.add_argument(
        "-4",
        "--gkde-tc",
        dest="method",
        help="use Gaussian KDE w/ thresheld convergence",
        action="store_const",
        const=4,
        default=argparse.SUPPRESS,
    )
    group.add_argument(
        "-5",
        "--vmf",
        dest="method",
        help="estimate a von Mises-Fisher distr. (using geomstats)",
        action="store_const",
        const=5,
        default=argparse.SUPPRESS,
    )
    group.add_argument(
        "-6",
        "--emmfa",
        dest="method",
        help="estimate an adaptive mixture of vMF (using geomstats)",
        action="store_const",
        const=6,
        default=argparse.SUPPRESS,
    )

    parser.add_argument(
        "-t",
        dest="terrain",
        metavar="FILE",
        help="terrain network to construct roads on",
        type=load_network,
        required=True,
    )

    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument(
        "-l",
        dest="leastcost",
        help="search the least cost network",
        action="store_true",
    )
    group.add_argument(
        "-e",
        dest="evidence",
        metavar="FILE",
        help="fit to given points of evidence",
        type=load_points,
    )
    group.add_argument(
        "-r",
        dest="reference",
        metavar="FILE",
        help="fit to given reference network",
        type=load_network,
    )

    parser.add_argument(
        "-a",
        dest="alpha",
        help="network formation parameter alpha",
        type=float,
        required=True,
    )
    parser.add_argument(
        "-p",
        dest="popfact",
        metavar="FACTOR",
        help="population size factor",
        type=int,
        default=defaults["population_factor"],
    )
    parser.add_argument(
        "-s",
        dest="survival",
        help="fraction of population to select",
        type=float,
        default=defaults["survival"],
    )
    parser.add_argument(
        "-g",
        dest="generations",
        help="number of generations to evaluate",
        type=int,
        default=defaults["generations"],
    )
    parser.add_argument(
        "-f",
        dest="comprep",
        metavar="FILE",
        help="load comp. repr. from a file",
        type=str,
        default=None,
    )
    parser.add_argument(
        "-d",
        dest="dryrun",
        help="do not write plans",
        action="store_true",
    )
    parser.add_argument(
        "--debug",
        help="save whole lineage to a file",
        action="store_true",
    )
    args = parser.parse_args()

    simulation = Simulation(args.terrain)

    if args.reference:
        simulation.set_reference(args.reference)
    elif args.evidence is not None:
        simulation.set_evidence(args.evidence)
    else:
        assert args.leastcost

    if args.comprep:
        comprep = CompositionalRepresentation.from_stats_file(args.comprep)
        simulation.set_comprep(comprep)
    else:
        # Estimate a compositional representation.
        # NOTE: This will take roughly as long as one generation.
        simulation.estimate_comprep(
            num_samples=args.popfact, alpha=args.alpha, verbose=True
        )

    if args.method == 1:
        distribution = MultivariateNormal
    elif args.method == 2:
        distribution = ThresheldMultivariateNormal
    elif args.method == 3:
        distribution = GaussianKDE
    elif args.method == 4:
        distribution = ThresheldGaussianKDE
    elif args.method == 5:
        if not args.use_ilr:
            parser.error("-5/--vmf requires -i/--ilr.")

        distribution = VonMisesFisher
    elif args.method == 6:
        if not args.use_ilr:
            parser.error("-6/--emmfa requires -i/--ilr.")

        distribution = EMMFA
    else:
        assert False, "Unexpected method number."

    eda = EDA(
        simulation=simulation,
        alpha=args.alpha,
        distribution=distribution,
        use_ilr=args.use_ilr,
        population_factor=args.popfact,
        survival=args.survival,
        generations=args.generations,
        save_progress=not args.dryrun,
        debug_progress=args.debug,
    )
    eda.run()
