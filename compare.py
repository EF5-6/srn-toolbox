#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Compare a network from grassgis2networkx.py to another or milestones."""

from itertools import combinations

import numpy as np
import shapely.geometry
import similaritymeasures
from more_itertools import pairwise

try:
    import cynetworkx as nx
except ModuleNotFoundError:
    import networkx as nx


class APSP_SPATIAL_DISTANCE:
    """Compares distances of pairwise shortest paths between selected nodes.

    This is a relatively expensive but very intuitive (for ``method="area"``)
    method to compare a predicted network to a reference network.
    """

    @staticmethod
    def dtw(x, y):
        """Return the first return value of similaritymeasures.dtw."""
        return similaritymeasures.dtw(x, y)[0]

    def __init__(
        self,
        method,
        resolution,
        network,
        nodes,
        edge_cost_key,
        edge_verts_key="_vertices",
    ):
        """Initialize with a fixed network to compare other networks to.

        :param str method:
            Comparison method to use; one of ``"pcm"``, ``"frechet"``,
            ``"area"``, ``"length"``, ``"dtw"``. See :mod:`similaritymeasures`.

        :param int resolution:
            Absolute tolerance used for simplifying the paths. See
            :meth:`shapely.geometry.LineString.simplify` with
            ``preserve_topology=False``.

        :param network:
            The fixed network that future networks should be compared to.

        :param nodes:
            A sequence of at least two nodes of the fixed network. For every
            pair of nodes, the shortest path between the nodes is compared
            spatially to a respective path in the networks being compared.

        :param str edge_cost_key:
            Attribute key storing edge costs for shortest path computations.

        :param str edge_verts_key:
            Attribute key storing vertex coordinates for each edge,
            denoting the actual spatial path that the edge represents.
        """
        if method == "pcm":
            self.measure = similaritymeasures.pcm
        elif method == "frechet":
            self.measure = similaritymeasures.frechet_dist
        elif method == "area":
            self.measure = similaritymeasures.area_between_two_curves
        elif method == "length":
            self.measure = similaritymeasures.curve_length_measure
        elif method == "dtw":
            self.measure = self.dtw
        else:
            raise ValueError(f"Unsupported method '{method}'.")

        if not all(node in network for node in nodes):
            raise ValueError("Not all given nodes are found in the graph.")

        self.method = method
        self.resolution = resolution
        self.network = network
        self.nodes = nodes

        self.k = len(nodes)
        self.paths = self._get_paths(
            network, nodes, edge_cost_key, edge_verts_key
        )

    def _get_paths(self, network, nodes, edge_cost_key, edge_verts_key):
        trees = {}
        for i in range(self.k - 1):
            u = nodes[i]
            trees[u] = nx.single_source_dijkstra_path(
                network, u, weight=edge_cost_key
            )

        paths = {}
        for u, v in combinations(nodes, 2):
            node_path = trees[u][v]

            vertex_path = []
            for edge in pairwise(node_path):
                vertex_path.extend(network.edges[edge][edge_verts_key])

            # Simplify the path to the given resolution/tolerance.
            line_string = shapely.geometry.LineString(vertex_path)
            simple_line_string = line_string.simplify(
                tolerance=self.resolution, preserve_topology=False
            )
            vertex_path = tuple(zip(*simple_line_string.xy))

            paths[u, v] = vertex_path

        return paths

    def __call__(
        self,
        network,
        nodes,
        edge_cost_key,
        edge_verts_key="_vertices",
        return_errors=False,
    ):
        """Compare a network to the fixed network.

        :param network:
            The network to compare with the fixed network.

        :param nodes:
            A sequence of nodes of the given network that correspond one-to-one
            to the nodes of the fixed network. It is not necessary that the
            nodes have the same coordinates.

        :param str edge_cost_key:
            Attribute key storing edge costs for shortest path computations.

        :param str edge_verts_key:
            Attribute key storing vertex coordinates for each edge,
            denoting the actual spatial path that the edge represents.

        :param bool return_errors:
            Whether to return also the list of per-connection errors whose sum
            is the total cost, ordered according to the base order.
        """
        if len(nodes) != self.k:
            raise ValueError("Node sequences must have the same length.")

        # Compute shortest paths between the given nodes.
        paths = self._get_paths(network, nodes, edge_cost_key, edge_verts_key)

        # Recover spatial errors for matching paths.
        errors = []
        for (p, q) in zip(combinations(self.nodes, 2), combinations(nodes, 2)):
            P = np.array(self.paths[p])
            Q = np.array(paths[q])
            errors.append(self.measure(P, Q))

        return (sum(errors), errors) if return_errors else sum(errors)


class MILESTONE_DISTANCE:
    """Measures distance between the network and a set of "milestone" points.

    The distance is computed as the mean squared Euclidean distance between the
    milestones' coordinates and the lines defined by the edges' vertices.
    """

    def __init__(self, milestones, *, resolution=None, coords_key="_coords"):
        """Initialize with a fixed collection of milestones to compare to.

        :param milestones:
            The fixed collection of milestone points that networks should be
            compared to. This can be a :obj:`dict` or pandas dataframe or a
            simple sequence of coordinates.

        :param int or None resolution:
            If not :obj:`None`, absolute tolerance used for simplifying the
            network. See :meth:`shapely.geometry.LineString.simplify` with
            ``preserve_topology=False``.

        :param str or None coords_key:
            Attribute key storing coordinates for each milestone. If this is
            :obj:`None`, then ``milestones`` is treated as a sequence of
            coordinates.
        """
        coords = milestones if coords_key is None else milestones[coords_key]
        self._milestones = shapely.geometry.MultiPoint(coords)
        self._resolution = resolution

    def __call__(self, network, *, edge_verts_key="_vertices"):
        """Compare a network to the milestone locations.

        :param network:
            The network to compare.

        :param str edge_verts_key:
            Attribute key storing vertex coordinates for each edge,
            denoting the actual spatial path that the edge represents.
        """
        # Convert network to a collection of lines.
        lines = shapely.geometry.MultiLineString(
            [vertices for _, _, vertices in network.edges(data=edge_verts_key)]
        )

        # Simplify the network if requested.
        if self._resolution:
            lines = lines.simplify(
                tolerance=self._resolution, preserve_topology=False
            )

        # Compute distance to network for every milestone.
        d = np.array(
            [point.distance(lines) for point in self._milestones.geoms]
        )

        # Return the mean squared distance.
        return np.mean(d**2)
