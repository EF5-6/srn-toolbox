#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Draw sequential road networks and/or reference data using plotly."""

import argparse
import json
from pathlib import Path

import plotly.graph_objects as go

from helper import load_network
from simulation import Simulation


COORDS_KEY = "_coords"
IS_SITE_KEY = "is_site"
SITE_NAME_KEYS = ("site_name", "site_TITLE", "name", "title")
VERTICES_KEY = "_vertices"


def _edge_coords(network, selector=None, *, reverse=False):
    x_seq, y_seq = [], []

    for _, _, d in network.edges(data=True):
        if selector and bool(d[selector]) is bool(reverse):
            continue

        vertices = d[VERTICES_KEY]
        x, y = zip(*vertices)

        x_seq.extend(x)
        y_seq.extend(y)

        x_seq.append(None)
        y_seq.append(None)

    return x_seq, y_seq


def _sites_data(network):
    x, y, names = [], [], []

    for _, d in network.nodes(data=True):
        if not d[IS_SITE_KEY]:
            continue

        x.append(d[COORDS_KEY][0])
        y.append(d[COORDS_KEY][1])

        name = None

        for name_key in SITE_NAME_KEYS:
            if name_key in d:
                name = d[name_key]
                break

        names.append(name)

    return x, y, names


parser = argparse.ArgumentParser(
    description="Plot sequential road networks.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
choice = parser.add_mutually_exclusive_group()
option1 = choice.add_argument_group()
option2 = choice
option1.add_argument(
    "-t",
    dest="terrain",
    help="file containing the terrain network",
    type=load_network,
)
option1.add_argument(
    "-r",
    dest="reference",
    help="file containing the reference network",
    type=load_network,
)
option1.add_argument(
    "-e",
    dest="evidence",
    help="file containing evidence points",
    type=Path,
)
option2.add_argument(
    "-d",
    dest="directory",
    help="directory with terrain.pickle, reference.pickle, evidence.json",
    type=Path,
)
parser.add_argument(
    "-p",
    dest="plan",
    help="file containing the road construction plan",
    type=Path,
)

args = parser.parse_args()

if args.directory:
    args.terrain = args.directory / "terrain.pickle"
    args.reference = args.directory / "reference.pickle"
    args.evidence = args.directory / "evidence.json"

    if not args.terrain.exists():
        args.terrain = None
    else:
        args.terrain = load_network(args.terrain)

    if not args.reference.exists():
        args.reference = None
    else:
        args.reference = load_network(args.reference)

    if not args.evidence.exists():
        args.evidence = None

if not args.terrain and not args.reference:
    parser.error("need at least a terrain or a reference network")

fig = go.Figure()

# Draw a reference network.
if args.reference:
    x, y = _edge_coords(args.reference)

    fig.add_trace(
        go.Scatter(
            name="reference",
            mode="lines",
            x=x,
            y=y,
            line=dict(width=3, color="#ffc800"),
            hoverinfo="skip",
        )
    )

# Draw a terrain network.
if args.terrain:
    x, y = _edge_coords(args.terrain)

    fig.add_trace(
        go.Scatter(
            name="terrrain",
            mode="lines",
            x=x,
            y=y,
            line=dict(width=0.5, color="#aaa"),
            hoverinfo="skip",
        )
    )

    if args.plan:
        simulation = Simulation(args.terrain)
        plan = simulation.load_plan(args.plan)
        roads = simulation.evaluate(
            plan, "draw_tier", "draw_cost", return_network=True
        )

        x, y = _edge_coords(roads, "draw_tier")

        fig.add_trace(
            go.Scatter(
                name="prediction",
                mode="lines",
                x=x,
                y=y,
                line=dict(width=2, color="#444"),
                hoverinfo="skip",
            )
        )

# Draw evidence points.
if args.evidence:
    with open(args.evidence, "r") as f:
        evidence = json.load(f)

        # TODO: Evidence data on hover.
        x, y = zip(*evidence[COORDS_KEY].values())

        fig.add_trace(
            go.Scatter(
                name="evidence",
                mode="markers",
                x=x,
                y=y,
                marker=dict(
                    symbol=2,
                    size=6,
                    line_width=1.5,
                    line_color="#444",
                    color="red",
                ),
            )
        )

# Draw sites.
if args.terrain or args.reference:
    if args.terrain:
        x, y, names = _sites_data(args.terrain)
    else:
        x, y, names = _sites_data(args.reference)

    fig.add_trace(
        go.Scatter(
            name="site",
            mode="markers",
            x=x,
            y=y,
            marker=dict(
                size=8,
                line_width=1.5,
                line_color="#444",
                color="lightblue",
            ),
            text=names,
            textposition="bottom center",
        )
    )

fig.update_xaxes(
    showgrid=False,
    zeroline=False,
    showticklabels=False,
)

fig.update_yaxes(
    showgrid=False,
    zeroline=False,
    showticklabels=False,
    scaleanchor="x1",
)

fig.update_layout(
    plot_bgcolor="white",
    margin=dict(b=5, l=5, r=5, t=20),
    annotations=[
        dict(
            text="<br />".join(
                (
                    f"<b>Alpha:</b> {plan[1] if args.plan else None}",
                    f"<b>Terrain:</b> {args.terrain}",
                    f"<b>Reference:</b> {args.reference}",
                    f"<b>Evidence:</b> {args.evidence}",
                    f"<b>Plan:</b> {args.plan}",
                )
            ),
            font=dict(size=10),
            showarrow=False,
            align="left",
            xref="paper",
            yref="paper",
            x=1 / 200,
            y=0,
        )
    ],
)

fig.show(config=dict(showTips=False))
