#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Generate statistics on random sequential road networks."""

import argparse
import os
import pickle
from datetime import timedelta
from itertools import combinations

import matplotlib.pyplot as plt
import numpy as np
from tqdm import tqdm

from helper import Timer
from helper import load_network
from population import Population
from simulation import Simulation

# Parse arguments.
parser = argparse.ArgumentParser(
    description="Generate random SRN statistics.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    "-t",
    dest="terrain",
    metavar="FILE",
    help="terrain network to construct roads on",
    type=load_network,
    required=True,
)
parser.add_argument(
    "-s",
    dest="samples",
    help="number of base samples",
    type=int,
    required=True,
)
parser.add_argument(
    "-a", dest="alpha", help="parameter alpha", type=float, default=None
)
parser.add_argument(
    "--transpose",
    dest="transpose",
    help="generate full transposition statistics in O(s l^2)",
    action="store_true",
)
parser.add_argument(
    "--adjacent",
    dest="adjacent",
    help="generate adjacent transposition statistics in O(s l)",
    action="store_true",
)
parser.add_argument(
    "-l",
    dest="leading",
    help="leading connections to consider for transposition",
    type=int,
    default=None,
)
parser.add_argument(
    "-w", dest="write", help="write population to file", action="store_true"
)
parser.add_argument(
    "--plot",
    dest="plot",
    help="plot the cost distribution",
    action="store_true",
)
args = parser.parse_args()

if args.adjacent and not args.transpose:
    parser.error("--adjacent requires -t/--transpose.")

if args.leading and not args.transpose:
    parser.error("-l requires -t/--transpose.")

# Establish simulation context.
simulation = Simulation(args.terrain)

# Setup empty population and a timer.
population = Population(simulation)
timer = Timer()

print(f"Evaluating {args.samples} samples for alpha={args.alpha}...")

with timer.measure(args.samples):
    population += (
        simulation.random_plan(args.alpha) for _ in range(args.samples)
    )

print(
    f"-------------------\n"
    f"Mean:   {population.mean_cost:11.4e}\n"
    f"Median: {population.median_cost:11.4e}\n"
    f"Stddev: {population.std:11.4e}\n"
    f"Skew:   {population.skew:+11.4e}\n"
    f"-------------------"
)

if args.transpose:
    print(
        f"Evaluating {'adjacent ' if args.adjacent else ''}transpositions "
        f"for every sample..."
    )

    n = args.leading if args.leading else len(population.best_plan.order)

    if args.adjacent:
        indices = tuple((i, i + 1) for i in range(n - 1))
    else:
        indices = tuple(combinations(range(n), 2))

    mean_dissimilarity = np.zeros((n, n))
    for plan in tqdm(population.generations[0].plans):
        simulation.set_target_plan(plan)

        # HACK: Work around the dissimilarity measure returning a constant
        #       nonzero value for equal plans.
        numeric_zero = simulation.evaluate(
            plan, "tier_numeric_zero", "cost_numeric_zero"
        )

        new_plans = []
        for i, j in indices:
            new_order = list(plan.order)
            new_order[i], new_order[j] = new_order[j], new_order[i]
            new_plans.append((new_order, plan.alpha))

        with timer.measure(len(new_plans)):
            population += new_plans

        for (i, j), dissimilarity in zip(indices, population.last.costs):
            if not np.equal(dissimilarity, numeric_zero):
                mean_dissimilarity[i, j] += dissimilarity

        print(".", end="", flush=True)

    if args.adjacent:
        mean_dissimilarity = np.diagonal(mean_dissimilarity, 1) / args.samples
    else:
        mean_dissimilarity += mean_dissimilarity.T
        mean_dissimilarity /= args.samples

    print(
        "",
        "Transposition mean dissimilarity:",
        mean_dissimilarity,
        sep="\n",
    )

    leading_str = f"_l{args.leading}" if args.leading else ""
    filename = (
        f"{'a' if args.adjacent else ''}tmd_a{args.alpha}"
        f"_s{args.samples}{leading_str}_pid{os.getpid()}.pickle"
    )

    with open(filename, "wb") as f:
        pickle.dump(mean_dissimilarity, f)

    print(f"Saved this to {filename}.")

print(
    f"Evaluated {1 / timer.time_per_thing:.4f} plans/s, "
    f"{timedelta(seconds=int(timer.elapsed))} total."
)

if args.write:
    filename = f"stats_a{args.alpha}_s{args.samples}.pickle"

    with open(filename, "wb") as f:
        pickle.dump(population, f)

    print(f"Saved population as {filename}.")

if args.plot:
    print("Plotting results...")

    plt.hist(population.costs, bins=100)
    plt.show()
