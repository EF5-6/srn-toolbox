#!/usr/bin/env python3

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Plot a mean relative cost change vector produced by :mod:`statistics`."""

import argparse
import pickle
import sys
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
from numpy import log as ln

from comprep import CompositionalRepresentation

# Parse arguments.
parser = argparse.ArgumentParser(
    description="Plot adjacent transposition statistics.",
    formatter_class=argparse.ArgumentDefaultsHelpFormatter,
)
parser.add_argument(
    dest="files",
    metavar="FILE",
    nargs="+",
    help="files containing an adj. transp. mean distance vector",
    type=Path,
)
parser.add_argument(
    "-w",
    dest="write",
    metavar="OUTFILE",
    help="write plot to file instead of displaying it",
    type=Path,
    default=None,
)
parser.add_argument(
    "-m",
    dest="merge",
    metavar="OUTFILE",
    help="do not plot anything; just merge and output vectors",
    type=Path,
    default=None,
)
parser.add_argument(
    "-t",
    dest="transparent",
    help="draw no background",
    action="store_true",
)
args = parser.parse_args()

# Merge any number of mean relative adjacent transposition cost change vectors.
y = 0
for path in args.files:
    with open(path, "rb") as fp:
        y = y + pickle.load(fp)
y /= len(args.files)

if args.merge:
    print(f"Writing input average to {args.merge}...")

    with open(args.merge, "wb") as fp:
        pickle.dump(y, fp)

    sys.exit(0)

# Compute fit parameters and function.
params = CompositionalRepresentation.from_stats_vector(
    adj_trans_mean_rel_cost_change=y, verbose=True, return_params=True
)
k, h, beta, lbd, delta, gamma = params

# Recover the two regimes of the fit function.
x = np.arange(1, k, dtype=float)
middle = h + 0.5
xe = np.append(x[:h], middle)
xp = np.insert(x[h:], 0, middle)
fe = beta * np.exp(lbd * xe)
fp = delta * xp**gamma

# Prepare the plot.
nz = np.nonzero(y)
ylims = [np.min(y) - 0.1 * np.std(y), np.max(y) + 0.1 * np.std(y)]
ylims_log = [np.min(ln(0.5 * y[nz])), np.max(ln(2.0 * y[nz]))]

# Setup a figure.
fig = plt.figure(figsize=(8, 5.5))
plt.rcParams["text.usetex"] = True
plt.rcParams["text.latex.preamble"] = r"\usepackage{amsmath}"

# Plot the fit in linear space.
ax1 = fig.add_subplot(211)
ax1.set_title(
    "A functional fit of the decay of importance for late connections"
)
ax1.set_xlabel(r"index $i$ of a transposition (i, i + 1)")
ax1.set_ylabel(r"$\operatorname{d_\text{net}}(i, i + 1)$")
ax1.set_ylim(ylims)
ax1.scatter(x, y, marker=".", s=10, label="mean distance", color="black")
ax1.plot(xe, fe, label="exponential regime", color="#0a7782")
ax1.plot(xp, fp, label="power regime", color="#a6d81e")
ax1.legend()

# Plot the fit in logarithmic space.
ax2 = fig.add_subplot(223)
ax2.set_title("Logarithmic plot")
ax2.set_xlabel(r"index $i$")
ax2.set_ylabel(r"$\log{\operatorname{d_\text{net}}(i, i + 1)}$")
ax2.set_ylim(ylims_log)
ax2.scatter(
    x[nz], ln(y[nz]), marker=".", s=10, label="mean distance", color="black"
)
ax2.plot(xe, ln(fe), label="exponential regime", color="#0a7782")
ax2.plot(xp, ln(fp), label="power regime", color="#a6d81e")
ax2.legend()

# Plot the fit in log-log space.
ax3 = fig.add_subplot(224)
ax3.set_title("Log-log plot")
ax3.set_xlabel(r"$\log{i}$")
ax3.set_ylabel(r"$\log{\operatorname{d_\text{net}}(i, i + 1)}$")
ax3.set_ylim(ylims_log)
ax3.scatter(
    ln(x[nz]),
    ln(y[nz]),
    marker=".",
    s=10,
    label="mean distance",
    color="black",
)
ax3.plot(ln(xe), ln(fe), label="exponential regime", color="#0a7782")
ax3.plot(ln(xp), ln(fp), label="power regime", color="#a6d81e")
ax3.legend()

# Show or save the plot.
if args.write:
    plt.tight_layout()
    plt.savefig(args.write, transparent=args.transparent, bbox_inches="tight")
else:
    plt.show()
