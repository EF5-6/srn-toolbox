#!/usr/bin/env python3
# pylama:ignore=D213,E265,E0602

############################################################################
#
# MODULE:       v.net.trivialize
# AUTHOR(S):    Maximilian Stahlberg
# PURPOSE:      Obtain an abstract road network from points and lines.
# COPYRIGHT:    (C) 2022 Maximilian Stahlberg
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

#%module
#% description: Generates an abstract road network from points and lines.
#% keywords: vector, points, lines, network, reconstruction
#% overwrite: yes
#%end

#%Option
#% key: sites
#% type: string
#% required: yes
#% multiple: no
#% key_desc: name
#% description: Name of input vector map with sites to consider (points).
#% gisprompt: old,vector,vector
#%End

#%Option
#% key: roads
#% type: string
#% required: yes
#% multiple: no
#% key_desc: name
#% description: Name of input vector map with roads to consider (lines).
#% gisprompt: old,vector,vector
#%End

#%option
#% key: costs
#% type: string
#% required: no
#% multiple: no
#% key_desc: name
#% description: Name of input raster map containing grid cell cost information.
#% gisprompt: old,cell,raster
#%end

#%option
#% key: output
#% type: string
#% required: yes
#% multiple: no
#% key_desc: name
#% description: Name of output vector map to contain the network.
#% gisprompt: old,vector,vector
#%end

#%option
#% key: output_sites
#% type: string
#% required: no
#% multiple: no
#% key_desc: name
#% description: Name of output vector map to contain selected sites only.
#% gisprompt: old,vector,vector
#%end

#%option
#% key: sitesnap
#% type: double
#% required: yes
#% multiple: no
#% key_desc: value
#% description: Maximum distance for snapping sites onto roads.
#%end

#%option
#% key: roadsnap
#% type: double
#% required: yes
#% multiple: no
#% key_desc: value
#% description: Maximum distance for closing gaps between road segments.
#%end

#%flag
#% key: c
#% description: Clip sites and roads to computational region.
#%end

"""A module to trivialize road networks for GRASS GIS."""

import os

import grass.script as gs
from grass.pygrass.modules.shortcuts import database as db
from grass.pygrass.modules.shortcuts import general as g
from grass.pygrass.modules.shortcuts import raster as r
from grass.pygrass.modules.shortcuts import vector as v
from grass.pygrass.vector import VectorTopo

from gss import GrassScriptSession


def main():
    """Run the module."""
    gss = GrassScriptSession("triv")

    # Load options.
    input_sites = OPTIONS["sites"]
    input_roads = OPTIONS["roads"]
    input_costs = OPTIONS["costs"]
    site_snap = float(OPTIONS["sitesnap"])
    road_snap = float(OPTIONS["roadsnap"])
    output = OPTIONS["output"]
    output_sites = OPTIONS["output_sites"]
    clip = FLAGS["c"]
    overwrite = gs.overwrite()

    # -------------------------------------------------------------------------

    if clip:
        gss.message(_("Clipping sites and roads to computational region..."))

        sites = gss.vector("clipped_sites")
        roads = gss.vector("clipped_roads")

        v.clip(flags="r", input=input_sites, output=sites, quiet=True)
        v.clip(flags="r", input=input_roads, output=roads, quiet=True)
    else:
        sites = input_sites
        roads = input_roads

    # -------------------------------------------------------------------------

    # Load number of points in sites map.
    with VectorTopo(sites, mode="r") as sites_topo:
        num_sites = sites_topo.number_of("points")

    if not num_sites:
        gss.fatal(_("Vector map <%s> does not contain any points.") % sites)
    else:
        gss.message(_("Loaded %d sites from <%s>.") % (num_sites, sites))

    # Load number of lines in road map.
    with VectorTopo(roads, mode="r") as roads_topo:
        num_roads = roads_topo.number_of("lines")

    if not num_roads:
        gss.fatal(_("Vector map <%s> does not contain any lines.") % roads)
    else:
        gss.message(
            _("Loaded %d road segments from <%s>.") % (num_roads, roads)
        )

    # -------------------------------------------------------------------------

    gss.message(
        _("Connecting road segments within %g map units...") % road_snap
    )

    v.clean(
        tool="snap",
        input=roads,
        output=gss.vector("clean_roads"),
        threshold=road_snap,
        flags="c",  # Break and repeatedly remove duplicates and small angles.
        quiet=True,
    )

    # Recount roads.
    with VectorTopo(gss.vector("clean_roads"), mode="r") as roads_topo:
        new_num_roads = roads_topo.number_of("lines")

        if new_num_roads != num_roads:
            gss.message(_("Now counting %d road segments.") % new_num_roads)

        num_roads = new_num_roads

    # -------------------------------------------------------------------------

    gss.message(
        _("Selecting sites within %g map units of a road...") % site_snap
    )

    # NOTE: The following is necessary despite v.net's threshold argument
    #       because we want to identify input sites with connected sites.
    # TODO: See if performance can be increased.

    v.buffer(
        type="line",
        input=gss.vector("clean_roads"),
        output=gss.vector("road_coverage"),
        distance=site_snap,
        quiet=True,
    )

    v.select(
        atype="point",
        operator="within",
        ainput=sites,
        binput=gss.vector("road_coverage"),
        btype="area",
        output=gss.vector("nearby_sites"),
        quiet=True,
    )

    # Recount sites.
    with VectorTopo(gss.vector("nearby_sites"), mode="r") as sites_topo:
        new_num_sites = sites_topo.number_of("points")

        if new_num_sites < 2:
            gss.fatal(_("Need at least two nearby sites, got %d.") % num_sites)
        elif new_num_sites < num_sites:
            gss.message(
                _("Removed %d sites, %d left.")
                % (num_sites - new_num_sites, new_num_sites)
            )

        num_sites = new_num_sites

    # -------------------------------------------------------------------------

    if output_sites:
        gss.message(_("Writing selected sites to <%s>...") % output_sites)

        g.copy(
            vector=(gss.vector("nearby_sites"), output_sites),
            overwrite=overwrite,
            quiet=True,
        )

    # -------------------------------------------------------------------------

    gss.message(_("Connecting selected sites to road network..."))

    # Connect sites to roads, forming a network.
    v.net(
        operation="connect",
        threshold=site_snap,
        input=gss.vector("clean_roads"),
        points=gss.vector("nearby_sites"),
        output=gss.vector("network"),
        quiet=True,
    )

    # HACK: Remove added points so that they can be added back with category
    #       numbers matching node IDs.
    v.edit(
        tool="delete",
        map=gss.vector("network"),
        layer=2,
        ids="0-99999999999",
        type="point",
        quiet=True,
    )

    # Place additional points at intersections.
    with gss.vector_inplace("network") as (old, new):
        v.net(
            operation="nodes",
            input=old,
            output=new,
            flags="c",  # HACK: See above.
            quiet=True,
        )

    # -------------------------------------------------------------------------

    gss.message(
        _("Working around grass#1571 by removing existing attributes...")
    )

    # TODO: Once https://github.com/OSGeo/grass/issues/1571 is fixed, maintain
    #       the old attribute tables in addition to those added below.

    with gss.vector_inplace("network") as (old, new):
        v.category(
            option="del",
            layer=1,
            cat=-1,
            input=old,
            output=new,
            quiet=True,
        )

    try:
        v.db_droptable(
            map=gss.vector("network"),
            layer=1,
            flags="f",
            quiet=True,
        )
    except Exception:
        pass

    # -------------------------------------------------------------------------

    gss.message(_("Assigning unique categories..."))

    with gss.vector_inplace("network") as (old, new):
        v.category(
            option="add",
            layer=1,
            type="line",
            input=old,
            output=new,
            quiet=True,
        )

    # -------------------------------------------------------------------------

    gss.message(_("Adding new attribute tables..."))

    v.db_addtable(
        map=gss.vector("network"),
        table="lines",
        layer=1,
        key="cat",
        columns=(
            "approx_cost DOUBLE PRECISION",
            "distance REAL",
        ),
        quiet=True,
    )

    v.db_addtable(
        map=gss.vector("network"),
        table="nodes",
        layer=2,
        key="cat",
        columns=("is_site INTEGER", "site_cat INTEGER"),
        quiet=True,
    )

    # -------------------------------------------------------------------------

    gss.message(_("Identifying sites with network nodes..."))

    v.db_addcolumn(
        map=gss.vector("nearby_sites"),
        columns="__node__ INTEGER",
        quiet=True,
    )

    # Save closest node with each site.
    v.distance(
        from_=gss.vector("nearby_sites"),
        from_layer=1,
        from_type="point",
        to=gss.vector("network"),
        to_layer=2,
        to_type="point",
        upload="cat",
        column="__node__",
        quiet=True,
    )

    # -------------------------------------------------------------------------

    gss.message(_("Uploading site information to network nodes..."))

    # TODO: Just like v.net.junctions, add all site_ attributes.

    db.execute(
        sql=(
            "UPDATE {nodes}"
            " SET (is_site, site_cat)"
            " = (SELECT 1, cat FROM {sites}"
            "    WHERE {sites}.__node__ = {nodes}.cat)"
        ).format(
            nodes="nodes",
            sites=gss.vector("nearby_sites"),
        )
    )

    db.execute(
        sql=("UPDATE {nodes} SET is_site = 0 WHERE is_site IS NULL").format(
            nodes="nodes",
        )
    )

    # -------------------------------------------------------------------------

    gss.message(_("Uploading road information to network lines..."))

    # Save path length between end nodes.
    v.to_db(
        map=gss.vector("network"),
        layer=1,
        query_layer=1,
        type="line",
        option="length",
        units="meters",
        columns="length",
        quiet=True,
    )

    # Save the sinuousity (length/air distance).
    v.to_db(
        map=gss.vector("network"),
        layer=1,
        query_layer=1,
        type="line",
        option="sinuous",
        columns="sinuousity",
        quiet=True,
    )

    # Save air distance between end nodes.
    db.execute(
        sql=("UPDATE {lines} SET distance = length / sinuousity").format(
            lines="lines",
        )
    )

    # Compute and store approximate costs.
    if input_costs:
        v.to_rast(
            input=gss.vector("network"),
            layer=1,
            type="line",
            output=gss.raster("rasterized"),
            use="cat",
            quiet=True,
        )

        r.volume(
            input=input_costs,
            clump=gss.raster("rasterized"),
            centroids=gss.vector("centroids"),
            output=os.devnull,
            overwrite=True,
            quiet=True,
        )

        db.execute(
            sql=(
                "UPDATE {lines} "
                "SET approx_cost = ("
                "    SELECT sum FROM {centroids} "
                "    WHERE {lines}.cat = {centroids}.cat)".format(
                    lines="lines",
                    centroids=gss.vector("centroids"),
                )
            ),
            quiet=True,
        )

    # -------------------------------------------------------------------------

    # TODO: Optionally remove road segments not on any LCP between sites.

    # -------------------------------------------------------------------------

    gss.message(_("Writing to <%s>...") % output)

    g.copy(
        vector=(gss.vector("network"), output),
        overwrite=overwrite,
        quiet=True,
    )


if __name__ == "__main__":
    OPTIONS, FLAGS = gs.parser()
    main()
