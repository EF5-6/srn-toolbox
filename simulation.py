#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Evaluate and draw sequential road networks using networkx."""

import json
import random
from itertools import combinations

import numpy as np
from more_itertools import pairwise
from tqdm import tqdm

from compare import APSP_SPATIAL_DISTANCE, MILESTONE_DISTANCE
from comprep import CompositionalRepresentation, fallback_comprep

try:
    import cynetworkx as nx
except ModuleNotFoundError:
    import networkx as nx

TARGET_TYPE_COST = 0  # Minimize total costs.
TARGET_TYPE_REFERENCE = 1  # Fit to a reference network.
TARGET_TYPE_EVIDENCE = 2  # Fit to a set of evidence.


class Simulation:
    """Class to construct sequential road networks from construction plans."""

    @staticmethod
    def cost_function(base, tier, alpha):
        """Compute travel cost based on base cost and road tier."""
        if tier > 0:
            return alpha * base
        else:
            return base

    def __init__(
        self, terrain_network, is_site_key="is_site", cost_key="cost"
    ):
        """Initialize a sequential road network simulation instance.

        :param terrain_network:
            A networkx undirected maximal graph to build a sequential road
            network in.

        :param is_site_key:
            The key of a vertex attribute that denotes whether the vertex is a
            distinguished site as opposed to a junction point.

        :param cost_key:
            The key of an edge attribute that denotes its cost.
        """
        self._terrain_network = terrain_network.copy()  # NOTE: Copy.
        self._terrain_is_site_key = is_site_key
        self._terrain_cost_key = cost_key

        self._terrain_sites, self._terrain_sites_data = zip(
            *[
                (v, d)
                for v, d in self._terrain_network.nodes(data=True)
                if d[self._terrain_is_site_key]
            ]
        )

        self._connections = tuple(combinations(self._terrain_sites, 2))
        self._base_order = range(len(self._connections))

        self._target_type = TARGET_TYPE_COST
        self._target = None
        self._compare_to_target = None

        self._comprep = fallback_comprep(self.num_connections)

    @property
    def num_connections(self):
        """Report number of connections to establish."""
        return len(self._connections)

    @property
    def connections(self):
        """Report connections to establish."""
        return self._connections

    @property
    def base_order(self):
        """Report the neutral order."""
        return self._base_order

    def set_reference(
        self,
        reference_network,
        is_site_key="is_site",
        cost_key="length",
        site_id_key="site_cat",
        comparison_method="area",
        comparison_resolution=5000,
    ):
        """Set a target reference network to approximate.

        Unsets any target evidence.

        :param reference_network:
            A networkx undirected graph featuring the same distinguished sites
            as the terrain graph.

        :param is_site_key:
            The key of a vertex attribute that denotes whether the vertex is a
            distinguished site as opposed to a junction point.

        :param cost_key:
            The key of an edge attribute that denotes its cost. The costs must
            be measured as for the terrain network.

        :param site_id_key:
            A common key or a pair of distinct keys of vertex attributes that
            assign to each distinguished site of both the terrain and the
            reference network a unique identifier that can be used to identify
            sites in both networks with each other. If this is a list or tuple,
            then the first element is used as a key for the terrain network
            and the second element is used as a key for the reference network.

        :param str comparison_method:
            Method for determining the similarity of two shortest paths. See
            :class:`APSP_SPATIAL_DISTANCE.`

        :param int comparison_resolution:
            Maximum number of vertices to select along each path before
            comparing them.
        """
        k = len(self._terrain_sites)

        if isinstance(site_id_key, (tuple, list)):
            terrain_site_id_key, reference_site_id_key = site_id_key
        else:
            terrain_site_id_key = reference_site_id_key = site_id_key

        # Identify sites.
        reference_sites, reference_sites_data = zip(
            *[
                (v, d)
                for v, d in reference_network.nodes(data=True)
                if d[is_site_key]
            ]
        )

        if len(reference_sites) != k:
            raise ValueError(
                f"Terrain network has {k} distinguished sites "
                f"while reference network has {len(reference_sites)}."
            )

        # Lookup identifying attributes for both terrain and reference sites.
        terrain_site2id = {
            self._terrain_sites[i]: self._terrain_sites_data[i][
                terrain_site_id_key
            ]
            for i in range(k)
        }
        reference_site2id = {
            reference_sites[i]: reference_sites_data[i][reference_site_id_key]
            for i in range(k)
        }

        # Check if terrain and reference site identifiers match.
        terrain_site_ids = set(terrain_site2id.values())
        reference_site_ids = set(reference_site2id.values())

        if len(terrain_site_ids) != k:
            raise ValueError(
                f"The '{terrain_site_id_key}' attribute is not unique among "
                f"terrain sites."
            )

        if len(reference_site_ids) != k:
            raise ValueError(
                f"The '{reference_site_id_key}' attribute is not unique among "
                f"reference sites."
            )

        if terrain_site_ids != reference_site_ids:
            raise ValueError(
                f"Terrain and reference network sites can not be matched "
                f"using their '{terrain_site_id_key}' and "
                f"'{reference_site_id_key}' attributes, respectively."
            )

        # Match reference sites with terrain sites.
        r2t = dict(
            zip(
                sorted(reference_sites, key=reference_site2id.__getitem__),
                sorted(self._terrain_sites, key=terrain_site2id.__getitem__),
            )
        )

        assert set(r2t.keys()) == set(reference_sites)
        assert set(r2t.values()) == set(self._terrain_sites)

        # Sort reference sites such that they match the terrain sites.
        reference_sites = sorted(
            reference_sites, key=lambda t: self._terrain_sites.index(r2t[t])
        )

        assert tuple(reference_site2id[t] for t in reference_sites) == tuple(
            terrain_site2id[u] for u in self._terrain_sites
        )

        # Store reference network and setup a comparator.
        self._target_type = TARGET_TYPE_REFERENCE
        self._target = reference_network
        self._compare_to_target = APSP_SPATIAL_DISTANCE(
            method=comparison_method,
            resolution=comparison_resolution,
            network=reference_network,
            nodes=reference_sites,
            edge_cost_key=cost_key,
        )

    def set_evidence(self, evidence, *, resolution=None, coords_key="_coords"):
        """Set target evidence to fit to.

        Unsets any reference network.

        See :class:`compare.MILESTONE_DISTANCE` for arguments.
        """
        self._target_type = TARGET_TYPE_EVIDENCE
        self._target = evidence
        self._compare_to_target = MILESTONE_DISTANCE(
            evidence, resolution=resolution, coords_key=coords_key
        )

    def set_target_plan(
        self,
        plan,
        site_id_key="site_cat",
        comparison_method="area",
        comparison_resolution=5000,
    ):
        """Evaluate a plan and set it as the reference network.

        See :meth:`set_reference` for parameters and side effects.
        """
        network = self.evaluate(
            plan, "tier_target", "cost_target", return_network=True
        )

        self.set_reference(
            reference_network=network,
            is_site_key=self._terrain_is_site_key,
            cost_key=self._terrain_cost_key,
            site_id_key=site_id_key,
            comparison_method=comparison_method,
            comparison_resolution=comparison_resolution,
        )

    def set_comprep(self, comprep):
        """Use a compositional representation of orders fit to the instance.

        :param CompositionalRepresentation comprep:
            The definition of a compositional representation, matching the
            number of connections being established in the simulation.
        """
        if not isinstance(comprep, CompositionalRepresentation):
            raise TypeError(
                f"Argument must be a {CompositionalRepresentation.__name__} "
                f"instance."
            )

        if len(comprep) != self.num_connections:
            raise ValueError(
                f"The representation is for {len(comprep)} connections but "
                f"the simulation considers {self.num_connections}."
            )

        self._comprep = comprep

    def estimate_comprep(
        self, num_samples, alpha=None, *, apply=True, verbose=False
    ):
        """Estimate parameters for the compositional representation of plans.

        This uses the mean relative cost change (with cost according to the
        simlation context) as a distance measure in the space of networks.

        :param None or float alpha:
            The simulation parameter alpha to estimate for. :obj:`None` samples
            uniformly from [0, 1]. If the simulation is run for a particular
            value of alpha, then setting it here is strongly recommended.

        :param bool apply:
            Whether the estimate should be applied via :meth:`set_comprep`.

        :param bool verbose:
            Whether to report estimation progress to the console.

        :returns:
            A :class:`CompositionalRepresentation` instance to use with
            :meth:`set_comprep`.
        """
        from population import Population

        if verbose:
            print(
                f"# Generating adjacent transposition statistics for "
                f"alpha={alpha}:"
            )
            print(f"# Evaluating {num_samples} initial plans...")

        P = Population(self)
        P += (self.random_plan(alpha) for _ in range(num_samples))

        indices = tuple((i, i + 1) for i in range(self.num_connections - 1))

        if verbose:
            total = num_samples * len(indices)
            print(
                f"# Evaluating {num_samples} x {len(indices)} = {total} "
                f"adjacent transpositions..."
            )

        # Compute mean relative cost change for adj. transp. by first index.
        mrcc = np.zeros(len(indices))

        for cost, plan in tqdm(P.results, desc="# Progress"):
            new_plans = []

            for i, j in indices:
                new_order = list(plan.order)
                new_order[i], new_order[j] = new_order[j], new_order[i]
                new_plans.append((new_order, plan.alpha))

            P += new_plans

            for (i, j), new_cost in zip(indices, P.last.costs):
                rel_cost_change = abs(new_cost - cost) / cost
                mrcc[i] += rel_cost_change

            if verbose:
                print(".", end="", flush=True)

        mrcc /= num_samples

        comprep = CompositionalRepresentation.from_stats_vector(mrcc)
        comprep._origin = "generated by simulation context"

        if apply:
            self.set_comprep(comprep)

        return comprep

    def order2sequence(self, order):
        """Convert an order to a connection sequence."""
        return tuple(self._connections[i] for i in order)

    def sequence2order(self, sequence):
        """Convert a connection sequence to an order."""
        return tuple(self._connections.index(con) for con in sequence)

    # TODO: This is redundant with respect to Plan.from_file; refactor.
    def load_plan(self, filename):
        """Load a plan from a file."""
        with open(filename, "r") as f:
            plan = json.load(f)

        alpha = plan["alpha"]
        order = tuple(plan["order"])

        if len(order) != len(self._connections):
            raise ValueError(
                "File order does not match number of connections."
            )

        return order, alpha

    def random_plan(self, alpha=None):
        """Obtain a random road construction plan."""
        if alpha is None:
            alpha = random.uniform(0, 1)

        order = list(range(len(self._connections)))
        random.shuffle(order)
        order = tuple(order)

        return order, alpha

    def evaluate(
        self,
        plan,
        tier_key,
        cost_key,
        return_errors=False,
        return_network=False,
    ):
        """Construct and rate a road network within the terrain graph."""
        order, alpha = plan

        G = self._terrain_network
        base_cost_key = self._terrain_cost_key

        nx.set_edge_attributes(G, 0, tier_key)
        nx.set_edge_attributes(
            G,
            {
                (a, b): self.cost_function(G[a][b][base_cost_key], 0, alpha)
                for a, b in G.edges()
            },
            cost_key,
        )

        net_cost = 0
        positive_edges = set()

        for u, v in self.order2sequence(order):
            # Find shortest u-v-path.
            path_nodes = nx.shortest_path(G, u, v, cost_key)

            if self._target_type or return_network:
                positive_edges.update(pairwise(path_nodes))
            else:
                net_cost += sum(
                    G[a][b][cost_key] for a, b in pairwise(path_nodes)
                )

            # Update the network.
            for a, b in pairwise(path_nodes):
                G[a][b][tier_key] += 1
                G[a][b][cost_key] = self.cost_function(
                    G[a][b][base_cost_key], G[a][b][tier_key], alpha
                )

        if return_network:
            return G.edge_subgraph(positive_edges).copy()
        elif self._target_type == TARGET_TYPE_REFERENCE:
            # Compare with a reference network.
            return self._compare_to_target(
                network=G.edge_subgraph(positive_edges),
                nodes=self._terrain_sites,
                edge_cost_key=base_cost_key,
                return_errors=return_errors,
            )
        elif self._target_type == TARGET_TYPE_EVIDENCE:
            # Compare with a set of evidence.
            return self._compare_to_target(
                network=G.edge_subgraph(positive_edges)
            )
        else:
            # No target network given, minimize formation cost.
            assert self._target_type == TARGET_TYPE_COST
            return net_cost
