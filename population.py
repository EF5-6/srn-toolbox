#!/usr/bin/env python3
# pylama:ignore=D203,D213,E203

# -----------------------------------------------------------------------------
# Copyright (C) 2022 Maximilian Stahlberg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------------------

"""Manage road network construction plans and populations thereof.

The classes herein serve as an additional wrapper around a :class:`Simulation`
context that is tailored towards the implementation of evolutionary algorithms.
"""

import datetime
import json
import pickle
from multiprocessing import Pool
from pathlib import Path
from time import perf_counter

import composition_stats as cs
import numpy as np

from comprep import fallback_comprep


class Plan:
    """A road construction plan."""

    @classmethod
    def from_sequence(cls, sequence, alpha, simulation):
        """Construct a road construction plan given a connection sequence."""
        order = simulation.sequence2order(sequence)
        return cls(order, alpha, simulation)

    @classmethod
    def from_weights(cls, weights, alpha, simulation=None, increasing=True):
        """Construct a road construction plan from a weight vector.

        The order is obtained by sorting the weights.
        """
        order = sorted(
            range(len(weights)),
            key=weights.__getitem__,
            reverse=not increasing,
        )
        return cls(order, alpha, simulation)

    @classmethod
    def from_file(cls, filename, simulation=None):
        """Load a road construction plan from a file."""
        with open(filename, "r") as fp:
            content = json.load(fp)

        alpha = content["alpha"]
        order = content["order"]

        return cls(order, alpha, simulation)

    def __init__(self, order, alpha, simulation=None):
        """Construct a road construction plan."""
        self._simulation = simulation
        self._order = tuple(order) if not isinstance(order, tuple) else order
        self._positions = None
        self._alpha = alpha

    def __iter__(self):
        """Yield order and alpha."""
        yield self._order
        yield self._alpha

    def __len__(self):
        """Length of the order."""
        return len(self._order)

    def __lt__(self, other):
        """Allow sorting of (cost, plan) pairs."""
        return self._order < other._order

    @property
    def pair(self):
        """Give a pair of order and alpha."""
        return (self._order, self._alpha)

    @property
    def order(self):
        """Give the order, mapping positions to connection indices."""
        return self._order

    @property
    def positions(self):
        """Give the inverse order, mapping connection indices to positions."""
        if not self._positions:
            self._positions = tuple(
                self._order.index(i) for i in range(len(self._order))
            )

        return self._positions

    @property
    def alpha(self):
        """Give the parameter alpha."""
        return self._alpha

    @property
    def sequence(self):
        """Give the order converted to a connection sequence."""
        if not self._simulation:
            raise RuntimeError("No simulation context specified.")

        return self._simulation.order2sequence(self._order)

    @property
    def compositional(self):
        """Give the order as a compositional vector."""
        if self._simulation:
            return self._simulation._comprep.compositional(self)
        else:
            return fallback_comprep(len(self)).compositional(self)

    @property
    def clr(self):
        """Order as center log ratio transform of :attr:`compositional`."""
        return cs.clr(self.compositional)

    @property
    def ilr(self):
        """Order as isometric log ratio transform of :attr:`compositional`."""
        return cs.ilr(self.compositional)

    def save_as(self, filename):
        """Save the plan to a file."""
        content = dict(
            version=1,
            alpha=self.alpha,
            order=self.order,
            sequence=self.sequence if self._simulation else None,
        )

        with open(filename, "w") as fp:
            json.dump(content, fp)


# TODO: Consider renaming Population to Lineage.
class Population:
    """A single generation or lineage of road construction plans."""

    def __init__(
        self,
        simulation,
        save_best_as=None,
        save_best_to=None,
        save_as=None,
    ):
        """Initialize a population of road construction plans.

        :param Simulation simulation:
            The simulation context that the plans belong to.

        :param str or Path save_best_as:
            File to save the best plan found to.

        :param str or Path save_best_to:
            Directory to save the sequence of best plans found to.

        :param str or Path save_as:
            File to save the whole lineage to.
        """
        self._simulation = simulation
        self._save_best_as = Path(save_best_as) if save_best_as else None
        self._save_best_to = Path(save_best_to) if save_best_to else None
        self._save_as = Path(save_as) if save_as else None

        self._generations = []
        self._is_generation = False

        self._plans = []
        self._costs = []

        self._sorted_costs_and_plans = None
        self._start_time = perf_counter()

    def _register(self, plans, costs):
        old_best_cost = self.best_cost if self._plans else float("inf")

        self._plans.extend(plans)
        self._costs.extend(costs)

        self._sorted_costs_and_plans = tuple(
            sorted(zip(self._costs, self._plans))
        )
        self._sorted_plans = tuple(
            pair[1] for pair in self._sorted_costs_and_plans
        )

        new_best_cost = self.best_cost

        if not self._is_generation and new_best_cost < old_best_cost:
            if self._save_best_as:
                self.best_plan.save_as(self._save_best_as)

            if self._save_best_to:
                self._save_best_to.mkdir(exist_ok=True)
                elapsed = datetime.timedelta(
                    seconds=int(perf_counter() - self._start_time)
                )
                filename = (
                    self._save_best_to
                    / f"{new_best_cost:.4e}.{elapsed}.plan.json"
                )
                assert not filename.exists()
                self.best_plan.save_as(filename)

        if not self._is_generation and self._save_as:
            with open(self._save_as, "wb") as fp:
                pickle.dump(self, fp)

    def __len__(self):
        """Return the number of evaluated plans."""
        return len(self._plans)

    def __iadd__(self, plans):
        """Evaluate and add a generation of plans to the population.

        :param plans:
            A sequence of either Plan objects or simple (order, alpha) pairs.
        """
        plans = tuple(
            Plan(*plan, self._simulation)
            if not isinstance(plan, Plan)
            else plan
            for plan in plans
        )

        if not all(plan._simulation is self._simulation for plan in plans):
            raise ValueError("Plans belong to a different simulation context.")

        with Pool() as pool:
            costs = pool.starmap(
                self._simulation.evaluate,
                (
                    (plan.pair, f"tier_{net}", f"cost_{net}")
                    for net, plan in enumerate(plans)
                ),
            )

        generation = self.__class__(self._simulation)
        self._generations.append(generation)
        generation._is_generation = True
        generation._register(plans, costs)
        self._register(plans, costs)

        return self

    @property
    def generations(self):
        """Give a tuple of :class:`Population` representing all generations."""
        if self._is_generation:
            raise AttributeError("This is already a generation.")

        return tuple(self._generations)

    @property
    def last(self):
        """Another :class:`Population` containing only the last generation."""
        if self._is_generation:
            raise AttributeError("This is already a generation.")

        return self._generations[-1]

    @property
    def results(self):
        """Give all cost/plan pairs in the order plans were added."""
        return tuple(zip(self._costs, self._plans))

    @property
    def plans(self):
        """Give all plans in the order they were added."""
        return tuple(self._plans)

    @property
    def costs(self):
        """Give all costs in the order that associated plans were added."""
        return tuple(self._costs)

    @property
    def sorted_results(self):
        """Give all cost/plan pairs sorted by cost."""
        if not self._sorted_costs_and_plans:
            raise ValueError("No plans were added yet.")

        return self._sorted_costs_and_plans

    @property
    def sorted_plans(self):
        """Give all plans sorted by cost."""
        if not self._sorted_plans:
            raise ValueError("No plans were added yet.")

        return self._sorted_plans

    @property
    def best_result(self):
        """Give a pair of best plan and cost seen so far."""
        if not self._sorted_costs_and_plans:
            raise ValueError("No plans were added yet.")

        return self._sorted_costs_and_plans[0]

    @property
    def best_plan(self):
        """Report the best plan seen so far."""
        return self.best_result[1]

    @property
    def best_cost(self):
        """Report the lowest cost seen so far."""
        return self.best_result[0]

    @property
    def mean_cost(self):
        """Report the sample mean cost."""
        return float(np.mean(self._costs))

    @property
    def median_cost(self):
        """Report the sample median cost."""
        return float(np.median(self._costs))

    @property
    def worst_cost(self):
        """Report the highest cost seen so far."""
        if not self._sorted_costs_and_plans:
            raise ValueError("No plans were added yet.")

        return self._sorted_costs_and_plans[-1][0]

    @property
    def std(self):
        """Report the standard deviation of the costs."""
        return float(np.std(self._costs))

    @property
    def skew(self):
        """Report the skew of the costs.

        Imports SciPy on access.
        """
        import scipy.stats

        return float(scipy.stats.skew(self._costs))
