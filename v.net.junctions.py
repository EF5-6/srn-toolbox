#!/usr/bin/env python3
# pylama:ignore=D213,E265,E0602

############################################################################
#
# MODULE:       v.net.junctions
# AUTHOR(S):    Maximilian Stahlberg
# PURPOSE:      Generate a junction network between fixed sites
# COPYRIGHT:    (C) 2022 Maximilian Stahlberg
#
#  This program is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

#%module
#% description: Generates a junction network between fixed sites.
#% keywords: vector, points, lines, network, reconstruction
#% overwrite: yes
#%end

#%Option
#% key: sites
#% type: string
#% required: yes
#% multiple: no
#% key_desc: name
#% description: Name of input vector map with sites to connect (points)
#% gisprompt: old,vector,vector
#%End

#%Option
#% key: extra
#% type: string
#% required: no
#% multiple: no
#% key_desc: name
#% description: Name of input vector map with extra points to connect (points)
#% gisprompt: old,vector,vector
#%End

#%option
#% key: costs
#% type: string
#% required: yes
#% multiple: no
#% key_desc: name
#% description: Name of input raster map containing grid cell cost information
#% gisprompt: old,cell,raster
#%end

#%option
#% key: output
#% type: string
#% required: yes
#% multiple: no
#% key_desc: name
#% description: Name of output vector map to contain the junction network
#% gisprompt: old,vector,vector
#%end

#%option
#% key: noise
#% type: double
#% required: no
#% multiple: no
#% key_desc: value
#% description: Maximum value of noise to add to the cost surface
#%end

#%option
#% key: memory
#% type: integer
#% required: no
#% multiple: no
#% key_desc: value
#% description: Maximum memory to use in megabytes
#%end

#%flag
#% key: k
#% description: Allow Knight moves (less distortion but more junctions)
#%end

#%flag
#% key: r
#% description: Temporarily set computational region to match cost surface
#%end

#%flag
#% key: a
#% description: Temporarily align computational region with cost surface
#%end

"""Junction network module for GRASS GIS.

A junction network is an undirected planar graph with a travel cost associated
with each edge that connects a number of given sites through auxiliary vertices
(junctions) such that (1) the least cost paths between the sites in the
junction network have the same cost as the least cost paths between the same
sites on the associated cost map and (2) whenever two or more (unique) least
cost paths between sites on the cost map share a part of their route, then the
least cost paths between the same sites in the junction network contain a
common subpath with total cost equal to the cost of the shared part.

Intuitively, the junction network is a maximal hypothetical road map between
the given sites assuming that roads are only built along shortest paths between
any two of the given sites. For network analysis, it can be superior to the
weighted complete graph due to the addtional topological information. In
particular, it is useful for approaches that work with a notion of shared costs
for jointly used path segments (e.g. road construction and maintenance costs).

For typical terrain where shortest paths are unique and tend to converge into
movement corridors, few junctions can be expected and the resulting planar
graph can be computationally attractive.
"""

# TODO: Allow moving relevant junctions to their associated site location.
# TODO: Once relevant bugs in the VectorTopo class of PyGRASS are fixed or a
#       workaround is found (see https://github.com/OSGeo/grass/issues/1571),
#       add a third layer to the output map with categories that map both
#       points and lines to all paths that they appear in. Then, the related
#       data can be removed from the point and line attribute tables, where it
#       is currently redundant. Comments marked with (X) are related.
# TODO: Add approx_costs column to line table, using the same calculation as
#       v.net.trivialize so that they are comparable (in particular, use
#       input_costs instead of costs).

import atexit
import multiprocessing
import os
from gettext import ngettext
from itertools import islice, repeat

import grass.script as gs
from grass.pygrass.messages import Messenger
from grass.pygrass.modules.interface.module import ParallelModuleQueue
from grass.pygrass.modules.shortcuts import database as db
from grass.pygrass.modules.shortcuts import general as g
from grass.pygrass.modules.shortcuts import raster as r
from grass.pygrass.modules.shortcuts import vector as v
from grass.pygrass.raster import RasterSegment
from grass.pygrass.vector import VectorTopo
from grass.pygrass.vector.geometry import Point


def chunked(iterable, n):
    """Split an iterable into size-limited chunks."""
    i = iter(iterable)
    s = tuple(islice(i, n))
    while s:
        yield s
        s = tuple(islice(i, n))


def cleanup():
    """Restore region and delete temporary maps."""
    if SAVED_REGION:
        g.region(region=SAVED_REGION, quiet=True)

    if TEMP_RAST_MAPS:
        g.remove(
            flags="f", quiet=True, type="raster", name=",".join(TEMP_RAST_MAPS)
        )

    if TEMP_VECT_MAPS:
        g.remove(
            flags="f", quiet=True, type="vector", name=",".join(TEMP_VECT_MAPS)
        )

    if TEMP_REGIONS:
        g.remove(
            flags="f", quiet=True, type="region", name=",".join(TEMP_REGIONS)
        )


def _make_temp_name(basename):
    return "junc_%d_%s" % (PID, basename)


def make_temp_rast_name(basename):
    """Create and track a temporary raster map name."""
    name = _make_temp_name(basename)
    assert name not in TEMP_RAST_MAPS, "Duplicate temporary raster map name."
    TEMP_RAST_MAPS.append(name)
    return name


def make_temp_vect_name(basename):
    """Create and track a temporary vector map name."""
    name = _make_temp_name(basename)
    assert name not in TEMP_VECT_MAPS, "Duplicate temporary vector map name."
    TEMP_VECT_MAPS.append(name)
    return name


def make_temp_region_name(basename):
    """Create and track a temporary region name."""
    name = _make_temp_name(basename)
    assert name not in TEMP_REGIONS, "Duplicate temporary region name."
    TEMP_REGIONS.append(name)
    return name


def save_initial_region():
    """Save initial region under a temporary name to restore it on cleanup."""
    global SAVED_REGION
    SAVED_REGION = make_temp_region_name("_saved")
    g.region(save=SAVED_REGION, quiet=True)


def main():
    """Run the module."""
    input_sites = OPTIONS["sites"]
    input_extra = OPTIONS["extra"]
    input_costs = OPTIONS["costs"]
    output = OPTIONS["output"]
    noise = float(OPTIONS["noise"]) if OPTIONS["noise"] else None
    overwrite = gs.overwrite()
    knights = FLAGS["k"]
    adjust_region = FLAGS["r"]
    align_region = FLAGS["a"]

    if OPTIONS["memory"]:
        memory = int(OPTIONS["memory"])
    else:
        import psutil

        # Half of total system memory.
        memory = int(psutil.virtual_memory().total / 1024**2 / 2)

    mem_per_thread = int(memory / THREADS)

    # Report memory limit.
    MSG.verbose(_("Using up to %d MB memory.") % memory)

    # Report number of threads used.
    if THREADS > 1:
        MSG.verbose(_("Using up to %d threads.") % THREADS)
        MSG.verbose(_("Using up to %d MB memory per thread.") % mem_per_thread)

    # Load number of sites and attribute names from input vector map.
    # TODO: Allow using a different layer for the points table.
    # TODO: Allow using a different site identifier than "cat".
    input_layer = 1
    with VectorTopo(input_sites, layer=input_layer, mode="r") as sites_topo:
        input_num_sites = sites_topo.number_of("points")

        if not input_num_sites:
            MSG.fatal(
                _("Vector map <%s> does not contain any points.") % input_sites
            )

        if sites_topo.table is None:
            MSG.fatal(
                "Vector map <%s> does not have an attribute table associated "
                "with layer %d." % (input_sites, input_layer)
            )

        input_attrs = dict(sites_topo.table.columns.items())

        if "cat" not in input_attrs:
            MSG.fatal(
                "The attribute table associated with layer %d of <%s> does not"
                " have a 'cat' column." % (input_layer, input_sites)
            )

    # Inform the user about the number of sites.
    MSG.message(
        ngettext(
            "Loaded %d site from <%s>.",
            "Loaded %d sites from <%s>.",
            input_num_sites,
        )
        % (input_num_sites, input_sites)
    )

    if input_extra:
        # Count extra points.
        with VectorTopo(input_extra, mode="r") as extra_topo:
            input_num_extra = extra_topo.number_of("points")

        if not input_num_extra:
            MSG.fatal(
                _("Vector map <%s> does not contain any points.") % input_extra
            )

        # Inform the user about the number of extra points.
        MSG.message(
            ngettext(
                "Loaded %d extra point from <%s>.",
                "Loaded %d extra points from <%s>.",
                input_num_extra,
            )
            % (input_num_extra, input_extra)
        )

    # If requested, change region to match cost surface.
    if adjust_region or align_region:
        save_initial_region()

        if adjust_region:
            g.region(raster=input_costs, zoom=input_costs)
            MSG.message("Region temporarily set to match cost surface.")
        else:
            g.region(align=input_costs)
            MSG.message("Region temporarily aligned with cost surface.")

    # -------------------------------------------------------------------------
    MSG.message("Clipping points to computational region...")
    # -------------------------------------------------------------------------

    good_sites = make_temp_vect_name("good_sites")
    points = make_temp_vect_name("points")

    v.clip(flags="r", input=input_sites, output=good_sites, quiet=True)

    if input_extra:
        good_extra = make_temp_vect_name("good_extra")
        v.clip(flags="r", input=input_extra, output=good_extra, quiet=True)
        v.patch(
            input="%s,%s" % (good_sites, good_extra), output=points, quiet=True
        )
    else:
        g.copy(vector=(good_sites, points), quiet=True)

    # Recount sites.
    with VectorTopo(good_sites, mode="r") as good_sites_topo:
        num_sites = good_sites_topo.number_of("points")

    # Warn when sites were removed.
    if num_sites < input_num_sites:
        lost = input_num_sites - num_sites
        MSG.warning(
            ngettext(
                "Removed %d site not within region, %d left.",
                "Removed %d sites not within region, %d left.",
                lost,
            )
            % (lost, num_sites)
        )

    # Check if there are at least two sites to work with.
    if num_sites < 2:
        MSG.fatal(_("Need at least two sites, got %d.") % k)

    if input_extra:
        # Recount extra points.
        with VectorTopo(good_extra, mode="r") as good_extra_topo:
            num_extra = good_extra_topo.number_of("points")

        # Warn when extra points were removed.
        if num_extra < input_num_extra:
            lost = input_num_extra - num_extra
            MSG.warning(
                ngettext(
                    "Removed %d extra point not within region, %d left.",
                    "Removed %d extra points not within region, %d left.",
                    lost,
                )
                % (lost, num_extra)
            )

    # Load all points.
    with VectorTopo(points, mode="r") as points_topo:
        points = tuple(p for p in points_topo if isinstance(p, Point))
        k = len(points)

    if noise:
        # ---------------------------------------------------------------------
        MSG.message(_("Adding noise to cost surface..."))
        # ---------------------------------------------------------------------

        costs = make_temp_rast_name("costs")

        r.mapcalc(
            "%s = %s + rand(0, %f)" % (costs, input_costs, noise),
            seed=1,
            quiet=True,
        )
    else:
        costs = input_costs

    # -------------------------------------------------------------------------
    MSG.message(_("Computing cumulative cost maps..."))
    # -------------------------------------------------------------------------

    cumulative_costs = []
    directions = []

    for i, site in enumerate(islice(points, k - 1)):
        MSG.percent(i, k - 1, 1)

        cumulative_costs.append(make_temp_rast_name("cumulative_costs_%d" % i))
        directions.append(make_temp_rast_name("directions_%d" % i))

        QUEUE.put(
            r.cost(
                input=costs,
                start_coordinates=site.coords(),
                output=cumulative_costs[-1],
                outdir=directions[-1],
                quiet=True,
                run_=False,
                flags=("k" if knights else "") + "b",
                memory=mem_per_thread,
            )
        )

    QUEUE.wait()
    MSG.percent(1, 1, 1)

    # -------------------------------------------------------------------------
    MSG.message(_("Computing pairwise least cost paths..."))
    # -------------------------------------------------------------------------

    # NOTE: 'raw_trees' is a list of least cost path trees with pairwise
    #       disjoint sets of root-leaf paths. The number of such paths
    #       decreases from k - 1 for raw_trees[0] to 1 for raw_trees[k - 2].

    raw_trees = []
    done_paths, num_paths = 0, k * (k - 1) // 2

    for i, site in enumerate(islice(points, k - 1)):
        MSG.percent(done_paths, num_paths, 10)

        raw_trees.append(make_temp_vect_name("raw_tree_%d" % i))

        QUEUE.put(
            r.path(
                input=directions[i],
                start_coordinates=[
                    points[j].coords() for j in range(i + 1, k)
                ],
                vector_path=raw_trees[-1],
                raster_path=None,
                format="bitmask",
                quiet=True,
                run_=False,
            )
        )
        done_paths += k - (i + 1)

    QUEUE.wait()
    MSG.percent(1, 1, 10)

    # -------------------------------------------------------------------------
    MSG.message(_("Annotating least cost paths..."))
    # -------------------------------------------------------------------------

    trees = []
    path_index2tree_index = []
    path_index2endpoint_cats = []
    path_cat_offset = 0

    for i in range(k - 1):
        MSG.percent(i, k - 1, 10)

        num_paths = k - (i + 1)

        trees.append(make_temp_vect_name("tree_%d" % i))

        path_index2tree_index.extend(repeat(i, num_paths))
        path_index2endpoint_cats.extend(
            {points[i].cat, points[j].cat} for j in range(i + 1, k)
        )

        # NOTE: Existing path cats (produced by r.path) are assigned in reverse
        #       order to the line ids of each tree. Therefor, the "path index"
        #       tracked here coincides with the unique category numbers
        #       assigned below (shifted by one) but it does not coincide with
        #       line IDs in the multinetwork formed later.
        QUEUE.put(
            v.category(
                input=raw_trees[i],
                output=trees[-1],
                option="sum",
                cat=path_cat_offset,
                quiet=True,
                run_=False,
            )
        )

        path_cat_offset += num_paths

    QUEUE.wait()
    MSG.percent(1, 1, 10)

    assert path_cat_offset == len(path_index2tree_index)
    assert path_cat_offset == len(path_index2endpoint_cats)

    # network_num_paths = path_cat_offset

    # -------------------------------------------------------------------------
    MSG.message(_("Merging least cost paths..."))
    # -------------------------------------------------------------------------

    multinetwork = make_temp_vect_name("multinetwork")

    # TODO: See if this can be parallelized recursively.
    v.patch(input=trees, output=multinetwork, quiet=True)

    # -------------------------------------------------------------------------
    MSG.message(_("Computing the junction network..."))
    # -------------------------------------------------------------------------

    network_1 = make_temp_vect_name("network_1")
    network_2 = make_temp_vect_name("network_2")
    network_3 = make_temp_vect_name("network_3")
    network = make_temp_vect_name("network")

    # Break at intersections and remove duplicate line segments.
    v.clean(
        input=multinetwork,
        tool=("break", "rmdupl"),
        output=network_1,
        # quiet=True,
    )

    # Analyze the new edges (lines).
    line_index2path_indices = []
    line_index2endpoint_coords = []

    with VectorTopo(network_1, layer=1, mode="r") as topo:
        for line in topo.viter("lines"):
            # Recover least cost paths that the edge is involved in.
            line_index2path_indices.append(
                sorted(
                    line.c_cats.contents.cat[cat_num] - 1
                    for cat_num in range(line.c_cats.contents.n_cats)
                )
            )

            # Recover endpoint coordinates.
            u, w = line.nodes()
            line_index2endpoint_coords.append((u.coords(), w.coords()))

    num_lines = len(line_index2path_indices)

    # Get a mapping from edges to one least cost path tree that they appear in.
    # NOTE: Tree indices coincide with cumulative cost map indices.
    line_index2tree_index = tuple(
        path_index2tree_index[line_index2path_indices[line_index][0]]
        for line_index in range(num_lines)
    )

    # Get a surjective mapping from trees to edges that they cover.
    tree_index2line_indices = tuple([] for tree_index in range(k - 1))
    for line_index, tree_index in enumerate(line_index2tree_index):
        tree_index2line_indices[tree_index].append(line_index)

    # (X) Get a mapping from edges to categories of points it connects.
    line_index2path_endpoint_cats = tuple(
        tuple(path_index2endpoint_cats[i] for i in path_indices)
        for path_indices in line_index2path_indices
    )

    # (X) Remove path categories from layer 1.
    v.category(
        input=network_1, output=network_2, option="del", cat=-1, quiet=True
    )

    # (X) Add unique line categories to layer 1.
    v.category(input=network_2, output=network_3, option="add", quiet=True)

    # Place points at intersections. (Adds unique cats to layer 2.)
    v.net(
        input=network_3,
        operation="nodes",
        output=network,
        flags="c",
        quiet=True,
    )

    # -------------------------------------------------------------------------
    MSG.message(_("Recovering edge costs..."))
    # -------------------------------------------------------------------------

    line_index2cost = {}

    for tree_index, line_indices in enumerate(tree_index2line_indices):
        if not line_indices:
            continue

        cum_costs = cumulative_costs[tree_index]
        start_coords = (line_index2endpoint_coords[i][0] for i in line_indices)
        end_coords = (line_index2endpoint_coords[i][1] for i in line_indices)

        start_costs = []
        end_costs = []

        # TODO: Check if setting the region to the raster is correct (edge
        #       costs match path costs) also when the -r option is not given.
        segment = RasterSegment(cum_costs)
        segment.set_region_from_rast(cum_costs)
        segment.open()
        try:
            for coords in start_coords:
                start_costs.append(segment.get_value(coords))

            for coords in end_coords:
                end_costs.append(segment.get_value(coords))
        finally:
            segment.close()

        for i, line_index in enumerate(line_indices):
            line_index2cost[line_index] = abs(start_costs[i] - end_costs[i])

    assert len(line_index2cost) == num_lines

    # -------------------------------------------------------------------------
    MSG.message(_("Recovering approximate edge costs..."))
    # -------------------------------------------------------------------------

    raster_network = make_temp_rast_name("raster_network")
    centroids = make_temp_vect_name("centroids")

    v.to_rast(
        input=network,
        layer=1,
        type="line",
        output=raster_network,
        use="cat",
        quiet=True,
    )

    r.volume(
        input=input_costs,
        clump=raster_network,
        centroids=centroids,
        output=os.devnull,
        overwrite=True,
        quiet=True,
    )

    # -------------------------------------------------------------------------
    MSG.message(_("Identifying the original sites..."))
    # -------------------------------------------------------------------------

    # Create an attribute table for 'good_sites'.
    v.db_addcolumn(
        map=good_sites,
        columns=(
            "node INTEGER",  # Associated node.
            "offset DOUBLE PRECISION",  # Distance to associated node.
        ),
        quiet=True,
    )

    # Save location for each site so that nodes can later be moved.
    v.to_db(
        map=good_sites, option="coor", columns=("east", "north"), quiet=True
    )

    # Save closest node and its distance for each site.
    v.distance(
        from_=good_sites,
        from_layer=1,
        from_type="point",
        to=network,
        to_layer=2,
        to_type="point",
        upload=("cat", "dist"),
        column=("node", "offset"),
        quiet=True,
    )

    # -------------------------------------------------------------------------
    MSG.message(_("Writing to <%s>..." % output))
    # -------------------------------------------------------------------------

    g.copy(vector=(network, output), quiet=True, overwrite=overwrite)

    # -------------------------------------------------------------------------
    MSG.message(_("Populating attribute tables..."))
    # -------------------------------------------------------------------------

    # -- Layer 1: Lines with unique categories --

    # Create a table for edge attributes.
    v.db_addtable(
        map=output,
        layer=1,
        columns=(
            "multiplicity INT",  # Number of least cost paths this appears in.
            "sites TEXT",  # A list of pairs of sites connected through this.
            "cost REAL",  # Travel cost as experienced by the script.
            "approx_cost REAL",  # Travel cost from rasterized network.
            "distance REAL",  # Air distance.
        ),
        quiet=True,
    )

    # Drop all rows so we can use a single INSERT statement below.
    db.execute(sql=("DELETE FROM {edge_table}".format(edge_table=output)))

    # Fill the attribute table with site to site path information.
    # NOTE: Chunking works around OSError: [Errno 7] Argument list too long.
    for line_nums in chunked(range(num_lines), 100):
        db.execute(
            sql=("INSERT INTO {edge_table} ({keys}) VALUES {values}").format(
                edge_table=output,
                keys="cat, multiplicity, sites, cost",
                values=", ".join(
                    "({cat}, {multiplicity}, '{sites}', {cost})".format(
                        cat=(i + 1),
                        multiplicity=len(line_index2path_endpoint_cats[i]),
                        sites=str(line_index2path_endpoint_cats[i]),
                        cost=line_index2cost[i],
                    )
                    for i in line_nums
                ),
            )
        )

    # Store approximate costs.
    db.execute(
        sql=(
            "UPDATE {edge_table} "
            "SET approx_cost = ("
            "    SELECT sum FROM {centroids} "
            "    WHERE {edge_table}.cat = {centroids}.cat)".format(
                edge_table=output,
                centroids=centroids,
            )
        ),
        quiet=True,
    )
    db.execute(
        sql=(
            "UPDATE {edge_table} "
            "SET approx_cost = 0.0 WHERE approx_cost IS NULL".format(
                edge_table=output
            )
        ),
        quiet=True,
    )

    # Save path length between end nodes.
    v.to_db(
        map=output,
        layer=1,
        query_layer=1,
        type="line",
        option="length",
        units="meters",
        columns="length",
        quiet=True,
    )

    # Save the sinuousity (length/air distance).
    v.to_db(
        map=output,
        layer=1,
        query_layer=1,
        type="line",
        option="sinuous",
        columns="sinuousity",
        quiet=True,
    )

    # Save air distance between end nodes.
    db.execute(
        sql=("UPDATE {edge_table} SET distance = length / sinuousity").format(
            edge_table=output
        )
    )

    # -- Layer 2: Points with unique categories --

    # TODO: Upload the following node attributes:
    #       - Degree in the multigraph.
    #       - Sites it connects.

    # Create a table for node attributes.
    site_attrs = {"site_" + key: val for key, val in input_attrs.items()}
    v.db_addtable(
        map=output,
        layer=2,
        columns=(
            "is_site INTEGER",  # Whether this is a site or junction.
            "orig_east DOUBLE PRECISION",  # Original coordinates (east).
            "orig_north DOUBLE PRECISION",  # Original coordinates (north).
            "offset DOUBLE PRECISION",  # Distance to original position.
        )
        + tuple("%s %s" % (n, t.upper()) for n, t in site_attrs.items()),
        quiet=True,
    )

    # Assign to applicable nodes the site that they represent.
    db.execute(
        sql=(
            "UPDATE {node_table}"
            " SET (is_site, orig_east, orig_north, offset, {out_attrs})"
            " = (SELECT 1, east, north, offset, {in_attrs} FROM {site_table}"
            "    WHERE {site_table}.node = {node_table}.cat)"
        ).format(
            node_table=output + "_2",
            site_table=good_sites,
            in_attrs=", ".join(input_attrs),
            out_attrs=", ".join("site_" + name for name in input_attrs),
        )
    )
    db.execute(
        sql=(
            "UPDATE {node_table} SET is_site = 0 WHERE is_site IS NULL"
        ).format(
            node_table=output + "_2",
        )
    )

    # TODO: (X) See start of file.
    # # -- Layer 3: Lines and points with path categories --

    # # Create a table for information on site to site paths.
    # v.db_addtable(
    #     map=output,
    #     columns=(
    #         "site_cat_1 INT",  # Category of first site connected by path.
    #         "site_cat_2 INT",  # Category of second site connected by path.
    #     ),
    #     quiet=True,
    # )

    # # Drop all rows so we can use a single INSERT statement below.
    # db.execute(sql=("DELETE FROM {path_table}".format(path_table=output)))

    # # Store the endpoint categories in the attribute table.
    # db.execute(
    #     sql=("INSERT INTO {path_table} VALUES {values}").format(
    #         path_table=output,
    #         values=", ".join(
    #             "({cat}, {cat1}, {cat2})".format(
    #                 cat=(i + 1),
    #                 cat1=path_index2endpoint_cats[i][0],
    #                 cat2=path_index2endpoint_cats[i][1],
    #             )
    #             for i in range(network_num_paths)
    #         ),
    #     )
    # )


if __name__ == "__main__":
    OPTIONS, FLAGS = gs.parser()
    MSG = Messenger()
    PID = os.getpid()
    THREADS = multiprocessing.cpu_count()
    QUEUE = ParallelModuleQueue(nprocs=THREADS)
    TEMP_RAST_MAPS = []
    TEMP_VECT_MAPS = []
    TEMP_REGIONS = []
    SAVED_REGION = None

    atexit.register(cleanup)
    main()
